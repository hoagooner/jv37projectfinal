/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.controller.admin;

import com.mycompany.hotel_management.service.BookingService;
import com.mycompany.hotel_management.service.RoomService;
import com.mycompany.hotel_management.utils.SecurityUtils;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author abcdef
 */
@Controller
@RequestMapping("/admin")
public class AdminHomeController {

    @Autowired
    private RoomService roomService;

    @Autowired
    private BookingService bookingService;

    @RequestMapping(value = {"", "/home"})
    public String viewAdminHome(Model model) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = principal.toString();
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        }

        List<String> roles = SecurityUtils.getRolesOfUser();
        if (!CollectionUtils.isEmpty(roles) && (roles.contains("ROLE_ADMIN")
                || roles.contains("ROLE_SELLER") || roles.contains("ROLE_MANAGER"))) {

            model.addAttribute("now", new Date());
            model.addAttribute("bookedRooms", roomService.getBookedRoomToDay());
            model.addAttribute("roomQuantity", roomService.getRoomQuantity());
            model.addAttribute("guestQuantity", bookingService.getGuestQuantity());
            model.addAttribute("bookingQuantity", bookingService.getBookingQuantity());
            model.addAttribute("message", "Hello Admin: " + username);
            model.addAttribute("roomsByRoomType", roomService.getRoomsByRoomType());
            return "/admin/home";
        } else {
            return "redirect:/home";
        }
    }
}
