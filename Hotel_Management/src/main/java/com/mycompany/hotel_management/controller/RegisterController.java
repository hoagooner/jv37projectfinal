/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.controller;

import com.mycompany.hotel_management.entities.UserEntity;
import com.mycompany.hotel_management.service.RegisterService;
import com.mycompany.hotel_management.utils.SecurityUtils;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author OS
 */
@Controller
public class RegisterController {

    @Autowired
    private RegisterService registerService;

    @RequestMapping("/insert-register")
    public String home(Model model, @ModelAttribute("category") UserEntity user) {
        registerService.save(user);
        return "redirect:/home";
    }

    @RequestMapping("/register")
    public String registerPage(Model model) {
        List<String> roles = SecurityUtils.getRolesOfUser();
        if(!CollectionUtils.isEmpty(roles)){
            return "home";
        }
        model.addAttribute("re", new UserEntity());
        return "register";
    }

    @RequestMapping("/logout")
    public String logoutPage(Model model) {
        model.addAttribute("message", "Login Fail!!!");
        return "/";
    }
}
