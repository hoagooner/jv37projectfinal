/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.controller;

import com.mycompany.hotel_management.entities.BookingDetailEntity;
import com.mycompany.hotel_management.entities.BookingEntity;
import com.mycompany.hotel_management.entities.CreditCardEntity;
import com.mycompany.hotel_management.entities.PaymentEntity;
import com.mycompany.hotel_management.entities.RoomEntity;
import com.mycompany.hotel_management.entities.ServiceEntity;
import com.mycompany.hotel_management.enums.BookingStatus;
import com.mycompany.hotel_management.service.BookingService;
import com.mycompany.hotel_management.service.CreditCardService;
import com.mycompany.hotel_management.service.RoomService;
import com.mycompany.hotel_management.service.RoomTypeService;
import com.mycompany.hotel_management.service.ServiceService;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author OS
 */
@Controller
public class BookingController {

    @Autowired
    private HttpSession session;

    @Autowired
    private RoomService roomService;

    @Autowired
    private ServiceService serviceService;

    @Autowired
    private RoomTypeService roomTypeService;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private CreditCardService creditCardService;

    @Autowired
    private CheckoutController checkoutController;

    @RequestMapping("your-booking")
    public String viewBooking(Model model) {
        model.addAttribute("services", serviceService.getServices());
        return "booking";
    }

    @RequestMapping(value = "your-booking", method = RequestMethod.POST)
    public String viewBookingDetail(Model model,
            HttpServletRequest request) {
        String checkIn = request.getParameter("checkinStr");
        String checkOut = request.getParameter("checkoutStr");

        // kiem tra trung lai ngay da search
        if (!checkIn.equals(session.getAttribute("checkIn"))
                || !checkOut.equals(session.getAttribute("checkOut"))) {
            session.removeAttribute("bookingDetails");
        }

        BookingDetailEntity bookingDetail = new BookingDetailEntity();
        int roomId = Integer.valueOf(request.getParameter("roomId"));
        RoomEntity room = roomService.findById(roomId);
        bookingDetail.setRoom(room);
        Set<BookingDetailEntity> bookingDetails
                = (Set<BookingDetailEntity>) session.getAttribute("bookingDetails");
        if (bookingDetails == null) {
            bookingDetails = new HashSet<>();
        }
        String[] serviceIds = request.getParameterValues("serviceId");

        // set price 
        Date checkInDate = (Date) session.getAttribute("checkInDate");
        Date checkOutDate = (Date) session.getAttribute("checkOutDate");
        bookingDetail.setDiscount(room.getRoomType().resultDiscount());
        double price = room.getRoomType().salePrice() * diffDate(checkInDate, checkOutDate);

        if (serviceIds != null) {
            Set<ServiceEntity> services = new HashSet<>();
            for (int i = 0; i < serviceIds.length; i++) {
                ServiceEntity service = serviceService.findById(Integer.valueOf(serviceIds[i]));
                services.add(service);
                price += service.getPrice();
            }
            bookingDetail.setServices(services);
        }
        bookingDetail.setPrice(price);

        bookingDetails.add(bookingDetail);
        session.setAttribute("totalPrice", totalPrice(bookingDetails));
        session.setAttribute("bookingDetails", bookingDetails);
        session.setAttribute("checkIn", checkIn);
        session.setAttribute("checkOut", checkOut);
        return "redirect:/your-booking";
    }

    @RequestMapping("/your-booking/delete/{id}")
    public String deleteItem(Model model, HttpSession session,
            @PathVariable("id") int roomId) {
        String message = "";
        String type = "";
        Set<BookingDetailEntity> bookingDetails
                = (Set<BookingDetailEntity>) session.getAttribute("bookingDetails");

        if (bookingDetails != null) {
            for (BookingDetailEntity bookingDetail : bookingDetails) {
                if (bookingDetail.getRoom().getId() == roomId) {
                    bookingDetails.remove(bookingDetail);
                    break;
                }
            }
        }
        session.setAttribute("bookingDetails", bookingDetails);
        session.setAttribute("totalPrice", totalPrice(bookingDetails));
        return "redirect:/your-booking";
    }

    @RequestMapping("/your-booking/update/{id}")
    public String updateBookingDetail(Model model,
            HttpServletRequest request,
            @PathVariable("id") int roomId) {

        Set<BookingDetailEntity> bookingDetails
                = (Set<BookingDetailEntity>) session.getAttribute("bookingDetails");

        double price = roomService.findById(roomId).getRoomType().getPrice();
        //update service & price
        Set<ServiceEntity> services = new HashSet<>();
        String[] serviceIds = request.getParameterValues("serviceId");
        if (serviceIds != null) {
            for (int i = 0; i < serviceIds.length; i++) {
                ServiceEntity service = serviceService.findById(Integer.valueOf(serviceIds[i]));
                services.add(service);
                price += service.getPrice();
            }
        }
        if (bookingDetails != null) {
            for (BookingDetailEntity bookingDetail : bookingDetails) {
                if (bookingDetail.getRoom().getId() == roomId) {
                    bookingDetail.setServices(services);
                    bookingDetail.setPrice(price);
                    break;
                }
            }
        }
        session.setAttribute("bookingDetails", bookingDetails);
        session.setAttribute("totalPrice", totalPrice(bookingDetails));
        return "redirect:/your-booking";

    }

    public double totalPrice(Set<BookingDetailEntity> bookingDetails) {
        double result = 0;
        for (BookingDetailEntity bookingDetail : bookingDetails) {
            result += bookingDetail.getPrice();
        }
        return result;
    }

    public int diffDate(Date checkIn, Date checkOut) {
        long diff = checkOut.getTime() - checkIn.getTime();
        return (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    @RequestMapping("find-reservation")
    public String viewFormFind() {
        return "find-reservation";
    }

    @RequestMapping(value = "find-reservation", method = RequestMethod.POST)
    public String findReservation(Model model,
            HttpServletRequest request) {
        String bookingNumber = request.getParameter("bookingNumber");
        BookingEntity booking = bookingService.findByBookingNumber( bookingNumber);
        if (booking.getId() == 0 ) {
            model.addAttribute("type", "error-find");
            return "find-reservation";
        }
        if(booking.getStatus().equals(BookingStatus.CANCEL)){
            model.addAttribute("type", "error-cancel");
            model.addAttribute("bookingNumber",bookingNumber);
            return "find-reservation";
        }
        
        if(booking.getCheckIn().compareTo(new Date()) < 1){
            model.addAttribute("expired","true");
        }
        
        model.addAttribute("booking", booking);
        model.addAttribute("services", serviceService.getServices());
        return "find-reservation";
//        return "redirect:?strSearch=" + booking.getBookingNumber();
    }

    @RequestMapping(value = "cancel-booking", method = RequestMethod.POST)
    public String cancelBooking(Model model,
            HttpServletRequest request) throws MessagingException {

        int bookingId = Integer.valueOf(request.getParameter("bookingId"));

        BookingEntity booking = bookingService.findById(bookingId);
        
        PaymentEntity payment = booking.getPayments().iterator().next();

        CreditCardEntity creditCard = payment.getCreditCard();

        CreditCardEntity hotelCreditCard = creditCardService.findByCardNumber("1234567890");
        
        if("1234567890".equals(creditCard.getCardNumber())){
           
        }else{
        creditCard.setBalance(creditCard.getBalance() + payment.getAmount());
        hotelCreditCard.setBalance(hotelCreditCard.getBalance() - payment.getAmount());
        }

        checkoutController.sendMail(booking.getEmail(),
                "Cancel booking success",
                "booking " + booking.getBookingNumber() + " has been cancelled");

        creditCardService.save(creditCard);
        creditCardService.save(hotelCreditCard);
        booking.setStatus(BookingStatus.CANCEL);
        bookingService.save(booking);

        model.addAttribute("type", "success");
        return "find-reservation";
    }

}
