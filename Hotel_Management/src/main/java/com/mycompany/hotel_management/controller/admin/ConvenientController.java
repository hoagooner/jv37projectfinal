/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.controller.admin;

import com.mycompany.hotel_management.entities.ConvenientEntity;
import com.mycompany.hotel_management.entities.ImageEntity;
import com.mycompany.hotel_management.enums.CommonStatus;
import com.mycompany.hotel_management.service.ConvenientService;
import com.mycompany.hotel_management.service.ImageService;
import com.mycompany.hotel_management.service.RoomTypeService;
import com.mycompany.hotel_management.utils.UploadFile;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author abcdef
 */
@Controller
@RequestMapping("/admin/convenient")
public class ConvenientController {

    @Autowired
    private ConvenientService convenientService;
    
    @Autowired
    private ImageService imageService;

    @RequestMapping("/add")
    public String viewConvenientForm(Model model) {
        model.addAttribute("category", new ConvenientEntity());
        model.addAttribute("action", "add");
        model.addAttribute("statuses", CommonStatus.values());
        return "admin/convenient-form";
    }

    @RequestMapping("")
    public String viewConvenient(Model model) {
        model.addAttribute("convenients", convenientService.getConvenients());
        return "admin/convenient";
    }

    @RequestMapping(value = "/result", method = RequestMethod.POST)
    public String createPromotion(@ModelAttribute("convenient") ConvenientEntity convenient,
            HttpServletRequest request,
            @RequestParam("action") String action,
            @RequestParam("file") MultipartFile file) {
        convenientService.save(convenient);
        if (UploadFile.uploadFile(request, file) != null) {
            ImageEntity image = new ImageEntity();
            image.setName(UploadFile.uploadFile(request, file));
            image.setConvenient(convenient);
            imageService.save(image);
        }
        return "redirect:/admin/convenient";
    }

    @RequestMapping("/update/{id}")
    public String updateProduct(
            @PathVariable("id") int id,
            Model model) {
        ConvenientEntity covenient = convenientService.findById(id);
        if (covenient.getId() > 0) {
            model.addAttribute("convenient", covenient);
            model.addAttribute("action", "update");
            model.addAttribute("statuses", CommonStatus.values());

            return "admin/convenient-form";
        } else {
            return "redirect:admin/convenient?type=error&message=Not found covenient id: " + id;
        }
    }

    @RequestMapping("/search")
    public String search(Model model,
            @RequestParam("q") String str) {
        model.addAttribute("convenients", convenientService.findByName(str));
        return "admin/convenient";
    }

    @RequestMapping("/change-status/{id}")
    public String search(Model model,
            @PathVariable("id") int id) {
        ConvenientEntity convenient = convenientService.findById(id);
        if (convenient.getStatus().equals(CommonStatus.ACTIVE)) {
            convenient.setStatus(CommonStatus.INACTIVE);
        } else {
            convenient.setStatus(CommonStatus.ACTIVE);
        }
        convenientService.save(convenient);
        return "redirect:/admin/convenient";
    }

}
