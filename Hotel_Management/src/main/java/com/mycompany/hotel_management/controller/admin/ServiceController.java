/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.controller.admin;

import com.mycompany.hotel_management.entities.ImageEntity;
import com.mycompany.hotel_management.entities.ServiceEntity;
import com.mycompany.hotel_management.enums.CommonStatus;
import com.mycompany.hotel_management.service.ImageService;
import com.mycompany.hotel_management.service.ServiceService;
import com.mycompany.hotel_management.utils.UploadFile;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author abcdef
 */
@Controller
@RequestMapping("/admin/service")
public class ServiceController {

    @Autowired
    private ServiceService serviceService;

    @Autowired
    private ImageService imageService;

    @RequestMapping("/add")
    public String viewServiceForm(Model model) {
        model.addAttribute("service", new ServiceEntity());
        model.addAttribute("statuses", CommonStatus.values());

        model.addAttribute("action", "add");
        return "admin/service-form";
    }

    @RequestMapping("")
    public String viewService(Model model) {
        model.addAttribute("services", serviceService.getServices());
        return "admin/service";
    }

    @RequestMapping(value = "/result", method = RequestMethod.POST)
    public String createService(@ModelAttribute("service") ServiceEntity service,
            HttpServletRequest request,
            @RequestParam("file") MultipartFile file) {

        serviceService.save(service);
        if (UploadFile.uploadFile(request, file) != null) {
            ImageEntity image = new ImageEntity();
            image.setName(UploadFile.uploadFile(request, file));
            image.setService(service);
            imageService.save(image);
        }
        return "redirect:/admin/service";
    }

    @RequestMapping("/update/{id}")
    public String updateService(
            @PathVariable("id") int id,
            Model model) {
        ServiceEntity service = serviceService.findById(id);
        if (service.getId() > 0) {
            model.addAttribute("service", service);
            model.addAttribute("action", "update");
            model.addAttribute("statuses", CommonStatus.values());

            return "admin/service-form";
        } else {
            return "redirect:/home?type=error&message=Not found service id: " + id;
        }
    }

    @RequestMapping(value = "/image/delete/{id}", method = RequestMethod.POST)
    public String delelteImage(
            @PathVariable("id") int id,
            @RequestParam("id") int serviceId,
            @ModelAttribute("service") ServiceEntity service,
            HttpServletRequest request) {
        {
            imageService.delete(id);
//            request.getSession().setAttribute("service_", service);
            return "redirect:/admin/service/update/" + serviceId;
        }
    }

    @RequestMapping("/change-status/{id}")
    public String search(Model model,
            @PathVariable("id") int id) {
        ServiceEntity service = serviceService.findById(id);
        if (service.getStatus().equals(CommonStatus.ACTIVE)) {
            service.setStatus(CommonStatus.INACTIVE);
        } else {
            service.setStatus(CommonStatus.ACTIVE);
        }
        serviceService.save(service);
        return "redirect:/admin/service";
    }

    @RequestMapping("/search")
    public String search(Model model,
            @RequestParam("q") String str) {
        model.addAttribute("services", serviceService.findByName(str));
        return "admin/service";
    }

}
