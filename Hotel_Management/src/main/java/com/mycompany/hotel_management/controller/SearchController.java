/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.controller;

import com.mycompany.hotel_management.entities.BookingDetailEntity;
import com.mycompany.hotel_management.entities.RoomEntity;
import com.mycompany.hotel_management.entities.RoomTypeEntity;
import com.mycompany.hotel_management.service.ConvenientService;
import com.mycompany.hotel_management.service.RoomService;
import com.mycompany.hotel_management.service.RoomTypeService;
import com.mycompany.hotel_management.service.ServiceService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author abcdef
 */
@Controller
public class SearchController {

    @Autowired
    private RoomService roomService;

    @Autowired
    private RoomTypeService roomTypeService;

    @Autowired
    private ServiceService serviceService;

    @RequestMapping("/search")
    public String search(HttpSession session,
            @RequestParam("checkIn") String checkIn,
            @RequestParam("checkOut") String checkOut,
            @RequestParam("person") int person,
            Model model) throws ParseException {

        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        Date checkInDate = new Date();
        Date checkOutDate = new Date();
        try {
            checkInDate = formatter.parse(checkIn);
            checkOutDate = formatter.parse(checkOut);
        } catch (Exception e) {
            return "redirect:/?message=please select a date";
        }

        if (checkInDate.after(checkOutDate) || checkInDate.equals(checkOutDate)) {
            return "redirect:/?message=Check in date should be greater than Check out date";
        }

      
        List<Object[]> objects
                = roomTypeService.getAvailableRooms(person, checkInDate, checkOutDate);

        // list available room 
        List<RoomEntity> rooms = new ArrayList<>();
        for (Object[] object : objects) {
            RoomEntity room = roomService.findById((int) object[1]);
            rooms.add(room);
        }

        // list id room type
        List<Integer> ids = new ArrayList<>();
        for (Object[] object : objects) {
            int id = (int) object[0];
            if (!ids.contains(id)) {
                ids.add(id);
            }
        }

        Set<BookingDetailEntity> bookingDetails
                = (Set<BookingDetailEntity>) session.getAttribute("bookingDetails");
        // set rooms 
        List<RoomTypeEntity> roomTypes = new ArrayList<>();
        for (Integer id : ids) {
            RoomTypeEntity roomType = roomTypeService.findById((id));
            Set<RoomEntity> roomsSet = new HashSet<>();
            outer:
            for (RoomEntity room : rooms) {
                if (room.getRoomType().getName().equals(roomType.getName())) {
                    // loai cac phong da co trong gio hang
                    if (bookingDetails != null) {
                        for (BookingDetailEntity bookingDetail : bookingDetails) {
                            if (bookingDetail.getRoom().getId() == room.getId()) {
                                continue outer;
                            }
                        }
                    }
                    roomsSet.add(room);
                }
            }
            roomType.setRooms(roomsSet);
            roomTypes.add(roomType);
        }

        session.setAttribute("checkInDate", checkInDate);
        session.setAttribute("checkOutDate", checkOutDate);
        model.addAttribute("services", serviceService.getServices());
        model.addAttribute("roomTypes", roomTypes);
        model.addAttribute("checkIn",checkIn);
        model.addAttribute("checkOut",checkOut);
        session.setAttribute("person", person);
        return "result-room";
    }

    @RequestMapping("booking-now")
    public String bookingNow(
            HttpSession session, Model model) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, 1);
        String checkout = formatter.format(cal.getTime());
//        return search(session, formatter.format(new Date()), checkout, 1, model);
        return "redirect:/search?checkIn=" + formatter.format(new Date()) + "&checkOut=" + checkout + "&person=1";
    }
}
