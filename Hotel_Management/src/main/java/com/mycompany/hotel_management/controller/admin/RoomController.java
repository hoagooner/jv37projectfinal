/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.controller.admin;

import com.mycompany.hotel_management.entities.RoomEntity;
import com.mycompany.hotel_management.entities.RoomTypeEntity;
import com.mycompany.hotel_management.enums.CommonStatus;
import com.mycompany.hotel_management.service.RoomService;
import com.mycompany.hotel_management.service.RoomTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author abcdef
 */
@Controller
@RequestMapping("/admin/room")
public class RoomController {

    @Autowired
    private RoomService roomService;

    @Autowired
    private RoomTypeService roomTypeService;
    
    @InitBinder
    public void initBinder(WebDataBinder dataBinder)
    {
        StringTrimmerEditor ste = new StringTrimmerEditor(true);
        dataBinder.registerCustomEditor(String.class, ste);
    }

    @RequestMapping("/add")
    public String viewRoomForm(Model model) {
        model.addAttribute("room", new RoomEntity());
        model.addAttribute("statuses", CommonStatus.values());
        model.addAttribute("roomTypes", roomTypeService.getActiveRoomTypes());
        model.addAttribute("action", "add");
        return "admin/room-form";
    }

    @RequestMapping("")
    public String viewRoomTypes(Model model) {
        model.addAttribute("roomTypes", roomTypeService.getRoomTypesExists());
        model.addAttribute("roomsByRoomType", roomService.getRoomsByRoomType());
        return "admin/room";
    }

    @RequestMapping(value = "/result", method = RequestMethod.POST)
    public String createRoomType(@ModelAttribute("room") RoomEntity room) {
        roomService.save(room);
        return "redirect:/admin/room";
    }

    @RequestMapping("/update/{id}")
    public String updateRoomType(
            @PathVariable("id") int id,
            Model model) {
        RoomEntity room = roomService.findById(id);
        if (room.getId() > 0) {
            model.addAttribute("room", room);
            model.addAttribute("action", "update");
            model.addAttribute("roomTypes", roomTypeService.getRoomTypes());
            model.addAttribute("statuses", CommonStatus.values());
            return "admin/room-form";
        } else {
            return "redirect:/admin/room?type=error&message=Not found room id: " + id;
        }
    }

    @RequestMapping("/search")
    public String search(Model model,
            @RequestParam("q") String str) {
        if (str == null) {
            return "redirect:/admin/room";
        }
        int roomNumber = Integer.valueOf(str);
        model.addAttribute("roomsSearch", roomService.findByRoomNumber(roomNumber));
        return "admin/room";
    }
    
     @RequestMapping("/change-status/{id}")
    public String search(Model model,
            @PathVariable("id") int id) {
        RoomEntity room = roomService.findById(id);
        if (room.getStatus().equals(CommonStatus.ACTIVE)) {
            room.setStatus(CommonStatus.INACTIVE);
        } else {
            room.setStatus(CommonStatus.ACTIVE);
        }
        roomService.save(room);
        return "redirect:/admin/room";
    }

}
