/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.controller;

import com.mycompany.hotel_management.entities.UserEntity;
import com.mycompany.hotel_management.entities.UserRoleEntity;
import com.mycompany.hotel_management.enums.UserStatus;
import static com.mycompany.hotel_management.main.Main.encrytePassword;
import com.mycompany.hotel_management.service.UserRoleService;
import com.mycompany.hotel_management.service.UserService;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author nguye
 */
@Controller
public class AccountController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    public JavaMailSender emailSender;

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String signUp(Model model, @PathVariable(value = "email") String email) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = principal.toString();
        UserEntity userEntity = userService.findByEmail(email);
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        }
        model.addAttribute("username", username);
        model.addAttribute("userEntity", userEntity);
        model.addAttribute("message", "Hello: " + username);
        model.addAttribute("roles", userRoleService.getUserRole());
        return "register";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String signUpAccount(Model model,
            @ModelAttribute("signup") UserEntity userEntity,
            HttpServletRequest request,
            @RequestParam(value = "type", required = false) String type,
            @RequestParam(value = "message", required = false) String message) throws MessagingException {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = principal.toString();
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        }
        model.addAttribute("username", username);
        userEntity.setPassword(encrytePassword(userEntity.getPassword()));
        UUID uuid = UUID.randomUUID();
        userEntity.setUuid(uuid.toString());
        String[] roleIds = request.getParameterValues("role");
        if (roleIds != null) {
            Set<UserRoleEntity> roles = new HashSet<>();
            for (int i = 0; i < roleIds.length; i++) {
                UserRoleEntity userRole = userRoleService.getUserRoleById(Integer.valueOf(roleIds[i]));
                roles.add(userRole);
            }
            userEntity.setUserRoles(roles);
        }
        userService.save(userEntity);
//       send mail
        MimeMessage messagee = emailSender.createMimeMessage();
        boolean multipart = true;
        MimeMessageHelper helper = new MimeMessageHelper(messagee, multipart, "utf-8");
        String htmlMsg = "";
        htmlMsg = "<h3>Registration Success</h3>" + "<br><br>" + "Complete Registration! " + "<br><br>" + "To confirm your account, please click here : "
                + "<a href=\"http://localhost:8081/Hotel_Management/active/" + userEntity.getUuid() + "\">" + "active" + "</a>";
        String temp1 = "</table>";
        htmlMsg.concat(temp1);
        messagee.setContent(htmlMsg, "text/html");
        helper.setTo(userEntity.getEmail());
        helper.setSubject("Registration Success!!!");
        this.emailSender.send(messagee);
        model.addAttribute("type", type);
        model.addAttribute("message", message);
        return "login";
    }

    @RequestMapping(value = "/active/{uuid}")
    public String updateStatus(@PathVariable(value = "uuid") String uuid) {
        UserEntity userEntity = userService.findByUuid(uuid);
        userEntity.setStatus(UserStatus.ACTIVE);
        userService.save(userEntity);
        return "redirect:/login?type=success&message=Sign up success for account id: " + uuid;
    }
    
    @RequestMapping(value = "/change-password/{uuid}", method = RequestMethod.GET)
    public String changePassword(Model model,
            @PathVariable(value = "uuid") String uuid) {
        UserEntity userEntity = userService.findByUuid(uuid);
        model.addAttribute("userEntity", userEntity);
        model.addAttribute("roles", userRoleService.getUserRole());
        return "change-password";
    }

//    @RequestMapping(value = "/change-password/{uuid}", method = RequestMethod.GET)
//    public String changePassword(Model model,
//            @PathVariable(value = "uuid") String uuid) {
//        UserEntity userEntity = userService.findByUuid(uuid);
//        model.addAttribute("userEntity", userEntity);
//        model.addAttribute("roles", userRoleService.getUserRole());
//        return "change-password";
//    }

    @RequestMapping(value = "/change-password", method = { RequestMethod.GET, RequestMethod.POST })
    public String changePasswordResult(Model model,
            HttpServletRequest request,
            @ModelAttribute("userEntity") UserEntity userEntity) {
        String[] roleIds = request.getParameterValues("role");
        if (roleIds != null) {
            Set<UserRoleEntity> roles = new HashSet<>();
            for (int i = 0; i < roleIds.length; i++) {
                UserRoleEntity userRole = userRoleService.getUserRoleById(Integer.valueOf(roleIds[i]));
                roles.add(userRole);
            }
            userEntity.setUserRoles(roles);
        }
        userEntity.setStatus(UserStatus.ACTIVE);
        userEntity.setPassword(encrytePassword(userEntity.getPassword()));
        userService.save(userEntity);
        return "login";
    }

    @RequestMapping(value = "/changeAccount/{email}")
    public String viewAccount(Model model,
            @RequestParam(value = "type", required = false) String type,
            @RequestParam(value = "message", required = false) String message,
            @PathVariable(value = "email") String email) {

        UserEntity userEntity = userService.findByEmail(email);
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = principal.toString();
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        }
        model.addAttribute("username", username);

        if (userEntity.getId() > 0) {
            model.addAttribute("userEntity", userEntity);
            model.addAttribute("type", type);
            model.addAttribute("action", "update");
            model.addAttribute("message", message);
            userService.save(userEntity);
            return "user/account";
        } else {
            model.addAttribute("type", "fail");
            model.addAttribute("message", "email already exist.!!");

            return "redirect:/user/account?type=error&message=Not found ";
        }

    }

    @RequestMapping("/result")
    public String home(UserEntity userEntity,
            HttpServletRequest request) {
        userService.save(userEntity);

        return "redirect:/changeAccount/" + userEntity.getEmail();
    }

}
