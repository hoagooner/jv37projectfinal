/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.controller.admin;

import com.mycompany.hotel_management.entities.ConvenientEntity;
import com.mycompany.hotel_management.entities.ImageEntity;
import com.mycompany.hotel_management.entities.PromotionEntity;
import com.mycompany.hotel_management.entities.RoomTypeEntity;
import com.mycompany.hotel_management.enums.CommonStatus;
import com.mycompany.hotel_management.enums.RoomType;
import com.mycompany.hotel_management.service.ConvenientService;
import com.mycompany.hotel_management.service.ImageService;
import com.mycompany.hotel_management.service.PromotionService;
import com.mycompany.hotel_management.service.RoomTypeService;
import com.mycompany.hotel_management.utils.UploadFile;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author abcdef
 */
@Controller
@RequestMapping("/admin/room-type")
public class RoomTypeController {

    @Autowired
    private RoomTypeService roomTypeService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private ConvenientService convenientService;

    @Autowired
    private PromotionService promotionService;
    
    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        StringTrimmerEditor ste = new StringTrimmerEditor(true);
        dataBinder.registerCustomEditor(String.class, ste);
    }

    @RequestMapping("/add")
    public String viewRoomTypeForm(Model model) {
        model.addAttribute("roomType", new RoomTypeEntity());
        model.addAttribute("roomTypesEnum", RoomType.values());
        model.addAttribute("statuses", CommonStatus.values());
        model.addAttribute("convenients", convenientService.getConvenients());
        model.addAttribute("promotions", promotionService.getPromotions());
        model.addAttribute("action", "add");
        return "admin/room-type-form";
    }

    @RequestMapping("")
    public String viewRoomTypes(Model model) {
        model.addAttribute("convenients", convenientService.getConvenients());
        model.addAttribute("promotions", promotionService.getPromotions());
        model.addAttribute("roomTypes", roomTypeService.getRoomTypes());
        return "admin/room-type";
    }

    @RequestMapping(value = "/result", method = RequestMethod.POST)
    public String createRoomType(@ModelAttribute("roomType") RoomTypeEntity roomType,
            HttpServletRequest request,
            @RequestParam("file") MultipartFile file,
            @RequestParam("action") String action,
            Model model,
            BindingResult result) {

        String[] convenientIds = request.getParameterValues("convenient");
        if (convenientIds != null) {
            roomType.setConvenients(getConvenients_Req(convenientIds));
        }
        List<RoomType> roomTypesExists = roomTypeService.getRoomTypesExists();

        // k dc tao kieu phong da co
        if (action.equals("update")
                && !roomTypeService.findById(roomType.getId()).getName().equals(roomType.getName())) {
            if (roomTypesExists.contains(roomType.getName())) {
                model.addAttribute("error", "Room type already exists");
                return "forward:/admin/room-type/update/" + roomType.getId();
            }
        }
        if (action.equals("add"))
        {
            if (roomTypesExists.contains(roomType.getName())) {
                model.addAttribute("error", "Room type already exists");
                return "forward:/admin/room-type/add/";
            }
        }

        roomTypeService.save(roomType);

        if (UploadFile.uploadFile(request, file) != null) {
            ImageEntity image = new ImageEntity();
            image.setName(UploadFile.uploadFile(request, file));
            image.setRoomType(roomType);
            imageService.save(image);
        }

        return "redirect:/admin/room-type";
    }

    @RequestMapping("/update/{id}")
    public String updateRoomType(
            @PathVariable("id") int id,
            Model model) {
        RoomTypeEntity roomType = roomTypeService.findById(id);
        if (roomType.getId() > 0) {
            model.addAttribute("roomType", roomType);
            model.addAttribute("roomTypesEnum", RoomType.values());

            model.addAttribute("convenients", convenientService.getConvenients());
            model.addAttribute("convenientsOfRoomType", roomType.getConvenients());

            model.addAttribute("promotions", promotionService.getPromotions());
            model.addAttribute("promotionsOfRoomType", roomType.getPromotions());
            model.addAttribute("action", "update");
            model.addAttribute("statuses", CommonStatus.values());

            return "admin/room-type-form";
        } else {
            return "redirect:/home?type=error&message=Not found room-type id: " + id;
        }
    }

    @RequestMapping(value = "/image/delete/{id}", method = RequestMethod.POST)
    public String delelteImage(
            @PathVariable("id") int id,
            @RequestParam("id") int roomTypeId,
            @ModelAttribute("roomType") RoomTypeEntity roomType,
            HttpServletRequest request) {
        {
            imageService.delete(id);
            return "redirect:/admin/room-type/update/" + roomTypeId;
        }
    }

    @RequestMapping("/change-status/{id}")
    public String search(Model model,
            @PathVariable("id") int id) {
        RoomTypeEntity roomType = roomTypeService.findById(id);
        if (roomType.getStatus().equals(CommonStatus.ACTIVE)) {
            roomType.setStatus(CommonStatus.INACTIVE);
        } else {
            roomType.setStatus(CommonStatus.ACTIVE);
        }
        roomTypeService.save(roomType);
        return "redirect:/admin/room-type";
    }

    public Set<ConvenientEntity> getConvenients_Req(String[] convenientIds) {
        Set<ConvenientEntity> convenients = new HashSet<>();
        for (int i = 0; i < convenientIds.length; i++) {
            ConvenientEntity convenient
                    = convenientService.findById(Integer.valueOf(convenientIds[i]));
            convenients.add(convenient);
        }
        return convenients;
    }

    public Set<PromotionEntity> getPromotions_Req(String[] promotionIds) {
        Set<PromotionEntity> promotions = new HashSet<>();
        for (int i = 0; i < promotionIds.length; i++) {
            PromotionEntity promotion
                    = promotionService.findById(Integer.valueOf(promotionIds[i]));
            promotions.add(promotion);
        }
        return promotions;
    }
}
