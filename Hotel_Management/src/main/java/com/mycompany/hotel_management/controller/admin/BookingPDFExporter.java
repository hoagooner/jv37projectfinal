/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.controller.admin;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.mycompany.hotel_management.entities.BookingDetailEntity;
import com.mycompany.hotel_management.entities.BookingEntity;
import java.awt.Color;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.http.HttpServletResponse;


public class BookingPDFExporter {

    private List<BookingEntity> bookings;

    public BookingPDFExporter(List<BookingEntity> bookings) {
        this.bookings = bookings;
    }

    private void writeTableHeader(PdfPTable table) {
        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(Color.LIGHT_GRAY);
        cell.setPadding(5);

        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(Color.BLACK);

        cell.setPhrase(new Phrase("Re-Number", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("E-mail", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("P-Number", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("BookingDate", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("TotalPrice (VND)", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Status", font));
        table.addCell(cell);
    }
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    private void writeTableData(PdfPTable table) {
        for (BookingEntity booking : bookings) {
            table.addCell(booking.getBookingNumber().substring(0, 8));
            table.addCell(booking.getEmail());
            table.addCell(booking.getPhoneNumber().toString());
            table.addCell(formatter.format(booking.getBookingDate()).toString());
            table.addCell(String.valueOf(booking.getTotalPrice()));
            table.addCell(booking.getStatus().toString());
        }
    }

    private void writeTableHeader2(PdfPTable table) {
        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(Color.LIGHT_GRAY);
        cell.setPadding(5);

        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(Color.BLACK);

        cell.setPhrase(new Phrase("Room Type", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Price (VND)", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Discount", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Service Quantity", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Sub Total Price (VND)", font));
        table.addCell(cell);

    }

    private void writeTableData2(PdfPTable table, BookingEntity order) {
        for (BookingDetailEntity orderDtail : order.getBookingDetails()) {
            table.addCell(orderDtail.getRoom().getRoomType().getName().toString());
            table.addCell(String.valueOf(orderDtail.getRoom().getRoomType().getPrice()));
            table.addCell(String.valueOf(orderDtail.getDiscount()) + "%");
            table.addCell(String.valueOf(orderDtail.getServices().size()));
            table.addCell(String.valueOf(orderDtail.getPrice()));
        }
    }

    public void export(HttpServletResponse response) throws DocumentException, IOException {
        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document, response.getOutputStream());

        document.open();
        Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        font.setSize(18);
        font.setColor(Color.BLACK);

        Paragraph p = new Paragraph("List of Bookings", font);
        p.setAlignment(Paragraph.ALIGN_CENTER);
        document.add(p);
        PdfPTable table = new PdfPTable(6);
        table.setWidthPercentage(100f);
        table.setWidths(new float[]{2.0f, 4.0f, 2.3f, 2.0f, 1.8f, 2.2f});
        table.setSpacingBefore(10);
        writeTableHeader(table);
        writeTableData(table);
        document.add(table);

//      table bookingdetail
        Font font2 = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        font2.setSize(14);
        font2.setColor(Color.BLACK);
        for (BookingEntity order : bookings) {
            Paragraph p2 = new Paragraph("Booking: " + order.getBookingNumber().substring(0, 8) + " (" + order.getEmail() + ")", font2);
            p2.setAlignment(Paragraph.ALIGN_LEFT);
            p2.setSpacingBefore(10);
            document.add(p2);

            PdfPTable table2 = new PdfPTable(5);
            table2.setWidthPercentage(100f);
            table2.setWidths(new float[]{4.5f, 2.0f, 2.0f, 2.0f, 2.8f});
            table2.setSpacingBefore(10);
            writeTableHeader2(table2);
            writeTableData2(table2, order);
            document.add(table2);

            Font font3 = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
            font3.setSize(12);
            font3.setColor(Color.BLACK);
            Paragraph p3 = new Paragraph("Total Price: (VND)" + order.getTotalPrice(), font3);
            p3.setAlignment(Paragraph.ALIGN_RIGHT);
            document.add(p3);
        }

        document.close();

    }
}
