/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.controller.admin;

import com.lowagie.text.DocumentException;
import com.mycompany.hotel_management.controller.CheckoutController;
import com.mycompany.hotel_management.entities.BookingDetailEntity;
import com.mycompany.hotel_management.entities.BookingEntity;
import com.mycompany.hotel_management.entities.CreditCardEntity;
import com.mycompany.hotel_management.entities.PaymentEntity;
import com.mycompany.hotel_management.entities.ServiceEntity;
import com.mycompany.hotel_management.enums.BookingStatus;
import com.mycompany.hotel_management.service.BookingDetailService;
import com.mycompany.hotel_management.service.BookingService;
import com.mycompany.hotel_management.service.CreditCardService;
import com.mycompany.hotel_management.service.RoomService;
import com.mycompany.hotel_management.service.ServiceService;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author abcdef
 */
@Controller
@RequestMapping("/admin/reservation")
public class ReservationController {

    @Autowired
    private BookingDetailService bookingDetailService;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private ServiceService serviceService;

    @Autowired
    private CreditCardService creditCardService;

    @Autowired
    private CheckoutController checkoutController;

    @Autowired
    private RoomService roomService;

    @RequestMapping("")
    public String viewReservation(Model model) {
        model.addAttribute("bookings", bookingService.findAll());
        model.addAttribute("bookingStatus", BookingStatus.values());
        return "admin/reservation";
    }

    @RequestMapping("/change-status/{id}")
    public String changeStatus(@PathVariable("id") int id,
            @RequestParam("status") BookingStatus status) throws MessagingException {
        BookingEntity booking = bookingService.findById(id);
        if (status.toString().equals("CANCEL")) {
            cancelBooking(booking.getId());
        }
        booking.setStatus(status);
        bookingService.save(booking);
        return "redirect:/admin/reservation";
    }

    @RequestMapping("/{id}")
    public String addReservation(@PathVariable("id") int id,
            Model model) {
        BookingEntity booking = bookingService.findById(id);
        model.addAttribute("booking", booking);
        model.addAttribute("services", serviceService.getServices());
        model.addAttribute("paidPayment", totalPayment(booking));
        return "admin/reservation-form";
    }

    @RequestMapping("/update-service/{id}")
    public String updateService(@PathVariable("id") int bookingDetailId,
            Model model, HttpServletRequest request) {

        int bookingId = Integer.valueOf(request.getParameter("bookingId"));
        BookingDetailEntity bookingDetail = bookingDetailService.findById(bookingDetailId);
        double price = roomService.findById(bookingDetail.getRoom().getId()).getRoomType().salePrice();
        //update service & price
        Set<ServiceEntity> services = new HashSet<>();
        String[] serviceIds = request.getParameterValues("serviceId");
        if (serviceIds != null) {
            for (int i = 0; i < serviceIds.length; i++) {
                ServiceEntity service = serviceService.findById(Integer.valueOf(serviceIds[i]));
                services.add(service);
                price += service.getPrice();
            }
        }
        bookingDetail.setServices(services);
        bookingDetail.setPrice(price);
        bookingDetailService.save(bookingDetail);
        BookingEntity booking = bookingService.findById(bookingId);
        booking.setTotalPrice(totalPrice(booking));
        bookingService.save(booking);

        return "redirect:/admin/reservation/" + bookingId;
    }

    @RequestMapping(value = "/search-booking-date", method = RequestMethod.POST)
    public String searchOrderByDate(Model model,
            HttpServletRequest request,
            HttpSession session,
            @RequestParam("fromDate") String fromDate,
            @RequestParam("toDate") String toDate) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String url = request.getHeader("referer");

        Date fromDatee = simpleDateFormat.parse(fromDate);
        Date toDatee = simpleDateFormat.parse(toDate);
        List<BookingEntity> bookings = (List<BookingEntity>) bookingService.searchOrdersByDate(fromDatee, toDatee);
        List<BookingEntity> bookingsExport = new ArrayList<>();
        for (BookingEntity booking : bookings) {
            booking = bookingService.findById(booking.getId());
            bookingsExport.add(booking);
        }
        model.addAttribute("bookings", bookingsExport);
        session.setAttribute("booking-export", bookingsExport);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("bookingStatus", BookingStatus.values());
        return "admin/reservation";
    }

    @RequestMapping(value = "/export-file")
    public void exportToPDF(HttpServletResponse response,
            HttpSession session,
            Model model)
            throws DocumentException, IOException, ParseException {

        response.setContentType("application/pdf");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=export_" + currentDateTime + ".pdf";
        response.setHeader(headerKey, headerValue);
        List<BookingEntity> bookings = (List<BookingEntity>) session.getAttribute("booking-export");
        BookingPDFExporter exporter = new BookingPDFExporter(bookings);
        exporter.export(response);
    }

    public void cancelBooking(int bookingId) throws MessagingException {

        BookingEntity booking = bookingService.findById(bookingId);
        PaymentEntity payment = booking.getPayments().iterator().next();

        CreditCardEntity creditCard = payment.getCreditCard();
        creditCard.setBalance(creditCard.getBalance() + payment.getAmount());

        CreditCardEntity hotelCreditCard = creditCardService.findByCardNumber("1234567890");
        hotelCreditCard.setBalance(hotelCreditCard.getBalance() - payment.getAmount());

        checkoutController.sendMail(booking.getEmail(),
                "booking " + booking.getBookingNumber() + " has been cancelled",
                "booking " + booking.getBookingNumber() + " has been cancelled");

        creditCardService.save(creditCard);
        creditCardService.save(hotelCreditCard);
    }

    public double totalPrice(BookingEntity booking) {
        double totalPrice = 0;
        for (BookingDetailEntity bookingDetail : booking.getBookingDetails()) {
            totalPrice += bookingDetail.getPrice();
        }
        return totalPrice;
    }

    public double totalPayment(BookingEntity booking) {
        double totalPayment = 0;
        for (PaymentEntity payment : booking.getPayments()) {
            totalPayment += payment.getAmount();
        }
        return totalPayment;
    }

}
