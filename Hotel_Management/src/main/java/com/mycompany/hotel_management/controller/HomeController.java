/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.controller;

import com.mycompany.hotel_management.entities.RoomEntity;
import com.mycompany.hotel_management.entities.RoomTypeEntity;
import com.mycompany.hotel_management.entities.UserEntity;
import com.mycompany.hotel_management.service.ConvenientService;
import com.mycompany.hotel_management.service.RoomService;
import com.mycompany.hotel_management.service.RoomTypeService;
import com.mycompany.hotel_management.service.ServiceService;
import com.mycompany.hotel_management.service.UserService;
import com.mycompany.hotel_management.utils.SecurityUtils;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

    @Autowired
    private ConvenientService convenientService;

    @Autowired
    private RoomTypeService roomTypeService;

    @Autowired
    private RoomService roomService;

    @Autowired
    private ServiceService serviceService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = {"/home"}, method = RequestMethod.GET)
    public String welcomePage(Model model,
            @RequestParam(name = "type", required = false) String type,
            @RequestParam(name = "message", required = false) String message) {
        model.addAttribute("roomTypes", roomTypeService.getRoomTypes().size() > 4 ? roomTypeService.getRoomTypes().subList(1, 4) : roomTypeService.getRoomTypes());
        model.addAttribute("services", serviceService.getServices());
        model.addAttribute("type", type);
        model.addAttribute("message", message);

        List<String> roles = SecurityUtils.getRolesOfUser();
        if (!CollectionUtils.isEmpty(roles) && (roles.contains("ROLE_ADMIN")
                || roles.contains("ROLE_SELLER") || roles.contains("ROLE_MANAGER"))) {
            return "redirect:/admin/home";
        } else if (!CollectionUtils.isEmpty(roles) && (roles.contains("ROLE_USER"))) {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String username = principal.toString();
            if (principal instanceof UserDetails) {
                username = ((UserDetails) principal).getUsername();
                model.addAttribute("username", username);
            }
            return "home";
        }
        return "home";
    }

    @RequestMapping("")
    public String welcome(Model model,
            @RequestParam(name = "type", required = false) String type,
            @RequestParam(name = "message", required = false) String message) {
        model.addAttribute("roomTypes", roomTypeService.getRoomTypes().size() > 4 ? roomTypeService.getRoomTypes().subList(1, 4) : roomTypeService.getRoomTypes());
        model.addAttribute("services", serviceService.getServices());
        model.addAttribute("type", type);
        model.addAttribute("message", message);

        return "home";
    }

    @RequestMapping("login")
    public String redirectLogin() {
        List<String> roles = SecurityUtils.getRolesOfUser();
        if (!CollectionUtils.isEmpty(roles)) {
            return "home";
        }
        return "login";
    }

    @RequestMapping("/403")
    public String accessDenied(Model model) {
        return "403Page";
    }

    @RequestMapping("/room-type")
    public String viewRoom(Model model) {
        model.addAttribute("roomTypes", roomTypeService.getRoomTypes());
        return "room-type";
    }

    @RequestMapping("/room-detail/{id}")
    public String viewRoomDetail(Model model,
            @PathVariable("id") int id) {
        RoomTypeEntity roomType = roomTypeService.findById(id);
        model.addAttribute("roomType", roomType);
        model.addAttribute("services", serviceService.getServices());
        return "room-detail";
    }

    @RequestMapping("/room/{id}")
    public String viewRoomDetail(@PathVariable("id") int id,
            Model model) {
        RoomEntity rooms = roomService.findById(id);
        model.addAttribute("roomTypes", roomTypeService.getRoomTypes());
        model.addAttribute("convenients", convenientService.getConvenients());
        model.addAttribute("services", serviceService.getServices());
        model.addAttribute("room", rooms);
        model.addAttribute("rooms", roomService.getRooms());
        return "room-detail";
    }
}
