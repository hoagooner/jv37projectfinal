/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.controller.admin;

import com.mycompany.hotel_management.entities.ImageEntity;
import com.mycompany.hotel_management.entities.PromotionEntity;
import com.mycompany.hotel_management.entities.RoomTypeEntity;
import com.mycompany.hotel_management.enums.CommonStatus;
import com.mycompany.hotel_management.service.ImageService;
import com.mycompany.hotel_management.service.PromotionService;
import com.mycompany.hotel_management.service.RoomTypeService;
import com.mycompany.hotel_management.utils.UploadFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author abcdef
 */
@Controller
@RequestMapping("/admin/promotion")
public class PromotionController {

    @Autowired
    private PromotionService promotionService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private RoomTypeService roomTypeService;

    @RequestMapping("/add")
    public String viewPromotionForm(Model model,
            @ModelAttribute("promotion") PromotionEntity promotion) {
        model.addAttribute("promotion", (promotion != null) ? promotion : new PromotionEntity());
        model.addAttribute("statuses", CommonStatus.values());
        model.addAttribute("roomTypes", roomTypeService.getActiveRoomTypes());
        model.addAttribute("action", "add");
        return "admin/promotion-form";
    }

    @RequestMapping("")
    public String viewPromotion(Model model) {
        model.addAttribute("promotions", promotionService.getPromotions());
        return "admin/promotion";
    }

    @RequestMapping(value = "/result", method = RequestMethod.POST)
    public String updatePromo(@ModelAttribute("promotion") PromotionEntity promotion,
            HttpServletRequest request,
            Model model,
            @RequestParam("action") String action,
            @RequestParam("file") MultipartFile file,
            final RedirectAttributes redirectAttributes) {

        String url = request.getHeader("referer");
        // set roomtypes promo
        String[] roomTypeIds = request.getParameterValues("roomType");
        Set<RoomTypeEntity> roomTypes = new HashSet<>();
        Set<PromotionEntity> promotions = new HashSet<>();
        promotions.add(promotion);

        if (roomTypeIds != null) {
            for (int i = 0; i < roomTypeIds.length; i++) {
                RoomTypeEntity roomType
                        = roomTypeService.findById(Integer.valueOf(roomTypeIds[i]));
                roomTypes.add(roomType);
            }
        }

        List<RoomTypeEntity> invalidRoomTypes = new ArrayList<>();
        outer:
        for (RoomTypeEntity roomType : roomTypes) {
            if (roomType.getPromotions().size() > 0) {
                if (action.equals("update")) {
                    for (RoomTypeEntity roomTypeOfPromo : promotionService.findById(promotion.getId()).getRoomTypes()) {
                        if (roomTypeOfPromo.getId() == roomType.getId()) {
                            continue outer;
                        }
                    }
                }
                for (PromotionEntity promo : roomType.getPromotions()) {
                    if (promotion.getEndDate().compareTo(promo.getStartDate()) >= 0
                            && promotion.getStartDate().compareTo(promo.getEndDate()) <= 0) {
                        Set<PromotionEntity> Promos = new HashSet<>();
                        Promos.add(promo);
                        roomType.setPromotions(Promos);
                        invalidRoomTypes.add(roomType);
                        break;
                    }
                }
            }
        }

        if (invalidRoomTypes.size() > 0) {
            redirectAttributes.addFlashAttribute("promotion", promotion);
            redirectAttributes.addFlashAttribute("invalidRoomTypes", invalidRoomTypes);
            return "redirect:" + url;
        }

        for (RoomTypeEntity roomType : roomTypes) {
            roomType.setPromotions(promotions);
        }
        promotion.setRoomTypes(roomTypes);

        promotionService.save(promotion);
        if (UploadFile.uploadFile(request, file) != null) {
            ImageEntity image = new ImageEntity();
            image.setName(UploadFile.uploadFile(request, file));
            image.setPromotion(promotion);
            imageService.save(image);
        }
        return "redirect:/admin/promotion";
    }

    @RequestMapping("/update/{id}")
    public String updatePromo(HttpServletRequest request,
            @PathVariable("id") int id,
            Model model
    ) {
        PromotionEntity promotion = promotionService.findById(id);
        if (promotion.getId() > 0) {
            model.addAttribute("promotion", promotion);
            model.addAttribute("action", "update");
            model.addAttribute("statuses", CommonStatus.values());
            model.addAttribute("roomTypes", roomTypeService.getActiveRoomTypes());

            return "admin/promotion-form";
        } else {
            return "redirect:/home?type=error&message=Not found promotion id: " + id;
        }
    }

    @RequestMapping(value = "/image/delete/{id}", method = RequestMethod.POST)
    public String delelteImage(
            @PathVariable("id") int id,
            @RequestParam("id") int promotionId,
            @ModelAttribute("promotion") PromotionEntity promotion,
            HttpServletRequest request
    ) {
        {
            imageService.delete(id);
//            request.getSession().setAttribute("promotion_", promotion);
            return "redirect:/admin/promotion/update/" + promotionId;
        }
    }

    @RequestMapping("/search")
    public String search(Model model,
            @RequestParam("q") String str
    ) {
        if (str.length() == 0) {
            return "redirect:/admin/promotion";
        }
        model.addAttribute("promotions", promotionService.findByName(str));
        return "admin/promotion";
    }

}
