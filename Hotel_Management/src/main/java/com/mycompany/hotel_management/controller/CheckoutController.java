/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.controller;

import com.mycompany.hotel_management.entities.BookingDetailEntity;
import com.mycompany.hotel_management.entities.BookingEntity;
import com.mycompany.hotel_management.entities.CreditCardEntity;
import com.mycompany.hotel_management.entities.UserEntity;
import com.mycompany.hotel_management.enums.BookingStatus;
import com.mycompany.hotel_management.enums.BookingType;
import com.mycompany.hotel_management.enums.CommonStatus;
import com.mycompany.hotel_management.enums.Gender;
import com.mycompany.hotel_management.service.BookingDetailService;
import com.mycompany.hotel_management.service.BookingService;
import com.mycompany.hotel_management.service.CreditCardService;
import com.mycompany.hotel_management.service.PaymentService;
import com.mycompany.hotel_management.service.UserService;
import com.mycompany.hotel_management.utils.MailUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.mail.internet.MimeMessage;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author abcdef
 */
@Controller
public class CheckoutController {

    private MailUtils mailUtils;

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private BookingDetailService bookingDetailService;

    @Autowired
    private CreditCardService creditCardService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private UserService userService;
    
    @RequestMapping("check-out")
    public String checkout(Model model) {
        model.addAttribute("gender", Gender.values());
        return "check-out";
    }

    @RequestMapping(value = "/check-out", method = RequestMethod.POST)
    public String checkout(Model model, HttpSession session,
            HttpServletRequest request, @ModelAttribute("booking") BookingEntity booking) throws ParseException {

        Set<BookingDetailEntity> bookingDetails = (Set<BookingDetailEntity>) session.getAttribute("bookingDetails");
        if (bookingDetails == null) {
            return "/check-out";
        }
        Date checkIn = (Date) session.getAttribute("checkInDate");
        Date checkOut = (Date) session.getAttribute("checkOutDate");
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = principal.toString();
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        }
        UserEntity userEntity = userService.findByEmail(username);
        model.addAttribute("userEntity", userEntity);

        // payment
        String cardNumber = request.getParameter("cardNumber");
        CreditCardEntity creditCard = creditCardService.findByCardNumber(cardNumber);
        if (creditCard == null) {
            model.addAttribute("messageCredit", "invalid credit card number");
            model.addAttribute("booking", booking);
            model.addAttribute("gender", Gender.values());
            return "check-out";
        }

        booking.setTotalPrice((double) session.getAttribute("totalPrice"));

        try {
            paymentService.transferMoney(creditCard, booking);
        } catch (Exception ex) {
            model.addAttribute("messageTrans", ex.getMessage());
            model.addAttribute("booking", booking);
            model.addAttribute("gender", Gender.values());
            return "check-out";
        }

        for (BookingDetailEntity bookDetail : bookingDetails) {
            bookDetail.setBooking(booking);
            bookDetail.getRoom().setStatus(CommonStatus.INACTIVE);
        }
        UUID uuid = UUID.randomUUID();
        booking.setBookingDate(new Date());
        booking.setCheckIn(checkIn);
        booking.setCheckOut(checkOut);
        

        booking.setBookingNumber(uuid.toString().substring(0, 8));
        booking.setBookingDetails(bookingDetails);
        booking.setBookingType(BookingType.ONLINE);
        booking.setStatus(BookingStatus.BOOKED);

        try {
            // send mail
            String content = checkoutSuccessContent(booking, bookingDetails);
            String title = "Booking Success!";
            sendMail(booking.getEmail(), title, content);
        } catch (MessagingException ex) {
            return "check-out";
        }
        bookingService.save(booking);
        session.removeAttribute("bookingDetails");
        session.removeAttribute("totalPrice");

        model.addAttribute("type", "success");
        return "check-out";
    }

    public void sendMail(String email, String title, String content) throws MessagingException {
        MimeMessage messagee = emailSender.createMimeMessage();
        boolean multipart = true;
        MimeMessageHelper helper = new MimeMessageHelper(messagee, multipart, "utf-8");
        messagee.setContent(content, "text/html");
        helper.setTo(email);
        helper.setSubject(title);
        emailSender.send(messagee);
    }

    public String checkoutSuccessContent(BookingEntity booking, Set<BookingDetailEntity> bookingDetails) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        String htmlMsg = "";
        htmlMsg = "<h3>Booking Success</h3>"
                + "Hello " + booking.getFullName() 
                + "<h3>Booking Number: " + booking.getBookingNumber() + "</h3>"
                + "<h3>Guest: " + booking.getFullName() + "</h3>"
                + "<h3>Email: " + booking.getEmail() + "</h3>"
                + "<h3>Phone: " + booking.getPhoneNumber() + "</h3>"

                
                + "Click to view booking: <b>" + "<a href=\"http://localhost:8081/Hotel_Management/find-reservation?strSearch=" + booking.getBookingNumber() + "\">" + booking.getBookingNumber()+ "</a>"
                + "<table style=\"border: 1px solid black;  width: 100%;\n"

                + "Hello " + booking.getFullName() + "<table border ='1' style=\"border: 1px solid black;  width: 100%;\n"
                + "  border-collapse: collapse; \"><tr><td>Room Type </td><td>Room No.</td><td>Check In Date</td><td>Check Out Date</td><td>Total Price</td></tr>";
        for (BookingDetailEntity bookDetail : bookingDetails) {
            String checkin = sdf.format(bookDetail.getBooking().getCheckIn());
            String checkout = sdf.format(bookDetail.getBooking().getCheckOut());
            String temp = "<tr>"
                    + "<td>" + bookDetail.getRoom().getRoomType().getName() + "</td>"
                    + "<td>" + bookDetail.getRoom().getRoomNumber() + "</td>"
                    + "<td>" + checkin + "</td>"
                    + "<td>" + checkout + "</td>"
                    + "<td>" + bookDetail.getBooking().getTotalPrice() + "VND" + "</td>"
                    + "</tr>";
            htmlMsg = htmlMsg.concat(temp);
        }
        String temp1 = "</table><br>";
        htmlMsg.concat(temp1);
        return htmlMsg;
    }
}
