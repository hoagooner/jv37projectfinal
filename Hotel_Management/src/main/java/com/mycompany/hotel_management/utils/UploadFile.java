/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.utils;

import com.mycompany.hotel_management.entities.ImageEntity;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author abcdef
 */
public class UploadFile {

    public static String uploadFile(
            HttpServletRequest request,
            MultipartFile file) {
        String fileName = null;
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                ServletContext context = request.getServletContext();
                String pathUrl = context.getRealPath("/images");

                int index = pathUrl.indexOf("target");
                String pathFolder = pathUrl.substring(0, index) + "src\\main\\webapp\\upload\\images\\";
                Path path = Paths.get(pathFolder + file.getOriginalFilename());
                Files.write(path, bytes);
                fileName = file.getOriginalFilename();
            } catch (IOException e) {
                System.out.println(e);
            }
        } else {
            return fileName;
        }
        return fileName;
    }
    
}
