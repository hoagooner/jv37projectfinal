/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.repository;

import com.mycompany.hotel_management.entities.BookingEntity;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author OS
 */
@Repository
public interface BookingRepository extends CrudRepository<BookingEntity, Integer> {

    @Query("Select o From BookingEntity o where o.bookingDate between ?1 and ?2")
    public List<BookingEntity> findOrderByDate(Date date1, Date date2);

    @Query(value = "select count(*) from booking", nativeQuery = true)
    public Integer getBookingQuantity();

    @Query(value = "select count(*) from ( select count(email) from booking group by email) as x", nativeQuery = true)
    public Integer getGuestQuantity();

    public BookingEntity findByEmailAndBookingNumber(String email, String numberBooking);

    public BookingEntity findByEmail(String email);

    public BookingEntity findByBookingNumber(String bookingNumber);

    public void deleteByBookingNumber(String bookingNumber);
}
