/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.repository;

import com.mycompany.hotel_management.entities.PromotionEntity;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author abcdef
 */
@Repository
public interface PromotionRepository extends CrudRepository<PromotionEntity, Integer>{
    public List<PromotionEntity> findByNameContaining(String str);
    
    @Query("SELECT promo FROM PromotionEntity promo LEFT JOIN FETCH promo.images")
    @Override
    public List<PromotionEntity> findAll();
    
    @Query("SELECT promo FROM PromotionEntity promo LEFT JOIN FETCH promo.images"
            + " WHERE promo.name = (:name)")
    public List<PromotionEntity> findByName(@Param("name") String name);

    
}
