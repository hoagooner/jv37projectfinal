/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.repository;

import com.mycompany.hotel_management.entities.RoomEntity;
import com.mycompany.hotel_management.entities.RoomTypeEntity;
import com.mycompany.hotel_management.enums.CommonStatus;
import com.mycompany.hotel_management.enums.RoomType;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author abcdef
 */
@Repository
public interface RoomTypeRepository extends CrudRepository<RoomTypeEntity, Integer> {

    public List<RoomTypeEntity> findByName(RoomType name);

    public List<RoomTypeEntity> findByStatus(CommonStatus status);

//    @Query(value = " select room_type.* from room room join  room_type room_type on room.room_type_id = room_type.id\n"
//            + " where room_type.occupancy >= ?1 and room.status = 'ACTIVE' and \n"
//            + "            (room.id in (select a.id from room a join booking_detail b on a.id = b.room_id \n"
//            + "            	join booking c on b.booking_id = c.id where c.check_out < ?2 or c.check_in > ?3) \n"
//            + "          or room.id not in (select room_id from booking_detail )) order by room_type.price ASC", nativeQuery = true)
//    public List<RoomTypeEntity> getAvailableRooms(int person, Date checkIn, Date checkOut);
    @Query(value = "select room_type.id as roomTypeIds,room.id as roomIds  from room room join  room_type room_type on room.room_type_id = room_type.id\n"
            + " where room_type.occupancy >= ?1 and room.status = 'ACTIVE' and \n"
            + "  (room.id not in (select a.id from hotel.room a join hotel.booking_detail b on a.id = b.room_id\n"
            + "         	join hotel.booking c on b.booking_id = c.id where c.check_out between ?2 and  ?3\n"
            + "            and c.check_in between ?2 and ?3 )\n"
            + "          ) order by room_type.price ASC", nativeQuery = true)
    public List<Object[]> getAvailableRooms(int person, Date checkIn, Date checkOut);

    @Query("SELECT distinct r FROM RoomTypeEntity r LEFT JOIN FETCH r.images LEFT JOIN FETCH  r.convenients "
            + " LEFT JOIN FETCH r.rooms")
    public List<RoomTypeEntity> findAllQuery();

    @Query("SELECT distinct r FROM RoomTypeEntity r LEFT JOIN FETCH r.images  LEFT JOIN FETCH r.convenients "
            + " LEFT JOIN FETCH r.rooms"
            + " WHERE r.id = (:id)")
    public RoomTypeEntity findById(@Param("id") int id);

    @Query("SELECT distinct r FROM RoomTypeEntity r LEFT JOIN FETCH r.images LEFT JOIN FETCH r.convenients "
            + " LEFT JOIN FETCH r.rooms"
            + " WHERE r.name = (:name)")
    public List<RoomTypeEntity> findByName(@Param("name") String name);

    @Query("SELECT distinct r FROM RoomTypeEntity r LEFT JOIN FETCH r.images LEFT JOIN FETCH r.convenients "
            + " LEFT JOIN FETCH r.rooms"
            + " WHERE r.status = (:status)")
    public List<RoomTypeEntity> getRoomTypesByStatus(@Param("status") CommonStatus status);

    @Query("SELECT distinct r FROM RoomTypeEntity r LEFT JOIN FETCH r.images LEFT JOIN FETCH  r.convenients "
            + "JOIN FETCH r.rooms")
    public List<RoomTypeEntity> getRoomType_Room();

    @Query(value = "select * from room_type where room_type.id not in ( "
            + "select a.id from room_type a join promotion_room_type_relationship b on a.id = b.room_type_id "
            + "join promotion c on c.id = b.promotion_id  "
            + "where c.start_date < ?1 and c.end_date > ?2)", nativeQuery = true)
    public List<RoomTypeEntity> getRoomType_Promo(Date endDate, Date startDate);

}
