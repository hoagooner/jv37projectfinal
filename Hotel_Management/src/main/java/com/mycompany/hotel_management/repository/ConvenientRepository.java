/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.repository;

import com.mycompany.hotel_management.entities.ConvenientEntity;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author abcdef
 */
@Repository
public interface ConvenientRepository extends CrudRepository<ConvenientEntity, Integer> {
    
   public List<ConvenientEntity> findByNameContaining(String str);
   
   @Query("select c from ConvenientEntity c left join fetch  c.images where c.id =(:id)")
   public ConvenientEntity findById(@Param("id")int id);
   
   @Query("select c from ConvenientEntity c left join fetch  c.images ")
   @Override
   public List<ConvenientEntity> findAll();
}

