/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.repository;

import com.mycompany.hotel_management.entities.RoomEntity;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author abcdef
 */
@Repository
public interface RoomRepository extends JpaRepository<RoomEntity, Integer>, JpaSpecificationExecutor<RoomEntity> {

    List<RoomEntity> findByRoomNumber(int roomNumber);

    @Query("select rooms from RoomEntity rooms")
    @Override
    public List<RoomEntity> findAll();

    @Query(value = "select count(*) from room", nativeQuery = true)
    public Integer getRoomQuantity();

    @Query("select room from RoomEntity room where room.id = (:id)")
    public RoomEntity findById(@Param("id") int id);

    @Query(value="select a.* from room a join booking_detail b on a.id = b.room_id \n"
            + "join booking c on b.booking_id = c.id where CURDATE() between c.check_in and c.check_out"
            + " and c.status <> 'CANCEL'",nativeQuery = true)
    public List<RoomEntity> getBookedRoomToDay();

//    @Query("select new com.mycompany.hotel_management.repository.RoomCount(room, count(room.roomType)) "
//            + "from RoomEntity room join RoomTypeEntity room_type "
//            + "where room_type.occupancy >= (:person) and room.status = 'ACTIVE' "
//            + "and ( room.id in (select a.id from RoomEntity a join BookingDetailEntity b "
//            + "join BookingEntity c  where c.checkOut < (:checkIn) or c.checkIn > (:checkOut)) "
//            + "or room.id not in (select bd.room from BookingDetailEntity bd)) \n"
//            + "group by room.roomType")
//    List<RoomCount> countRoomByRoomType(@Param("person")int person,@Param("checkIn") Date checkIn,@Param("checkOut") Date checkOut);
     @Query(value = "select room.*  from room room join  room_type room_type on room.room_type_id = room_type.id\n"
            + " where room_type.occupancy >= ?1 and room.status = 'ACTIVE' and \n"
            + "  (room.id not in (select a.id from pj_hotel_ms.room a join pj_hotel_ms.booking_detail b on a.id = b.room_id\n"
            + "         	join pj_hotel_ms.booking c on b.booking_id = c.id where c.check_out between ?2 and  ?3\n"
            + "            and c.check_in between ?2 and ?3 )\n"
            + "          ) order by room_type.price ASC", nativeQuery = true)
    public List<RoomEntity> getAvailableRooms(int person, Date checkIn, Date checkOut);

}
