/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.entities;

import com.mycompany.hotel_management.enums.CommonStatus;
import com.mycompany.hotel_management.enums.RoomType;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author abcdef
 */
@Entity
@Table(name = "room_type")
public class RoomTypeEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Enumerated(EnumType.STRING)
//    @Column(length = 50, unique = true)
    private RoomType name = RoomType.SINGLE;

    private String description;

    private int size;

    private int occupancy;

    private double price;

    private int numberOfBed;

    @Enumerated(EnumType.STRING)
    private CommonStatus status = CommonStatus.ACTIVE;

    @ManyToMany(mappedBy = "roomTypes", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
    private Set<PromotionEntity> promotions;

    @ManyToMany(cascade = {CascadeType.REMOVE}, fetch = FetchType.LAZY)
    @JoinTable(name = "room_type_convenient_relationship",
            joinColumns = @JoinColumn(name = "room_type_id",
                    referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "convenient_id",
                    referencedColumnName = "id"))
    private Set<ConvenientEntity> convenients;

//    @OneToMany(mappedBy = "roomType", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
//    private Set<CommentEntity> comments;
//
//    @OneToMany(mappedBy = "roomType", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
//    private Set<CommentEntity> votes;
    @OneToMany(mappedBy = "roomType", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
    private Set<ImageEntity> images;

    @OneToMany(mappedBy = "roomType", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
    private Set<RoomEntity> rooms;

    public RoomTypeEntity() {
    }

    public Set<ImageEntity> getImages() {
        return images;
    }

    public void setImages(Set<ImageEntity> images) {
        this.images = images;
    }

    public Set<RoomEntity> getRooms() {
        return rooms;
    }

    public void setRooms(Set<RoomEntity> rooms) {
        this.rooms = rooms;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RoomType getName() {
        return name;
    }

    public void setName(RoomType name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public CommonStatus getStatus() {
        return status;
    }

    public int getNumberOfBed() {
        return numberOfBed;
    }

    public void setNumberOfBed(int numberOfBed) {
        this.numberOfBed = numberOfBed;
    }

    public void setStatus(CommonStatus status) {
        this.status = status;
    }

    public Set<PromotionEntity> getPromotions() {
        return promotions;
    }

    public void setPromotions(Set<PromotionEntity> promotions) {
        this.promotions = promotions;
    }

    public Set<ConvenientEntity> getConvenients() {
        return convenients;
    }

    public void setConvenients(Set<ConvenientEntity> convenients) {
        this.convenients = convenients;
    }

    public int getOccupancy() {
        return occupancy;
    }

    public void setOccupancy(int occupancy) {
        this.occupancy = occupancy;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int resultDiscount() {
        for (PromotionEntity promo : this.getPromotions()) {
            if (new Date().compareTo(promo.getStartDate()) >= 0
                    && new Date().compareTo(promo.getEndDate()) <= 0) {
                return promo.getDiscount();
            }
        }
        return 0;
    }
    
    public double salePrice(){
        return price*(1-resultDiscount()/100.0);
    }

}
