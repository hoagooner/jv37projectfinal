/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author abcdef
 */
//@Entity
//@Table(name = "vote")
public class VoteEntity implements Serializable{
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private int id;
//
//    private int vote;
//
//    @Column(name = "publish_date")
//    @Temporal(TemporalType.TIMESTAMP)
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
//    private Date voteDate;
//
//    @ManyToOne
//    @JoinColumn(name = "user_id")
//    private UserEntity user;
//
//    @ManyToOne
//    @JoinColumn(name = "room_type_id")
//    private RoomTypeEntity roomType;
//
//    public VoteEntity() {
//    }
//
//    public VoteEntity(int id, int vote, Date voteDate, UserEntity user, RoomTypeEntity roomType) {
//        this.id = id;
//        this.vote = vote;
//        this.voteDate = voteDate;
//        this.user = user;
//        this.roomType = roomType;
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public int getVote() {
//        return vote;
//    }
//
//    public void setVote(int vote) {
//        this.vote = vote;
//    }
//
//    public Date getVoteDate() {
//        return voteDate;
//    }
//
//    public void setVoteDate(Date voteDate) {
//        this.voteDate = voteDate;
//    }
//
//    public UserEntity getUser() {
//        return user;
//    }
//
//    public void setUser(UserEntity user) {
//        this.user = user;
//    }
//
//    public RoomTypeEntity getRoomType() {
//        return roomType;
//    }
//
//    public void setRoomType(RoomTypeEntity roomType) {
//        this.roomType = roomType;
//    }
//
}
