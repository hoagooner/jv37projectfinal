/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.service;

import com.mycompany.hotel_management.entities.ConvenientEntity;
import com.mycompany.hotel_management.repository.ConvenientRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author abcdef
 */
@Service
public class ConvenientService {

    @Autowired
    private ConvenientRepository convenientRepository;

    public void save(ConvenientEntity convenient) {
        convenientRepository.save(convenient);
    }

    public List<ConvenientEntity> getConvenients() {
        return convenientRepository.findAll();
    }

    public ConvenientEntity findById(int id) {
        return convenientRepository.findById(id);
    }

    public List<ConvenientEntity> findByName(String str) {
        List<ConvenientEntity> promotions = (List<ConvenientEntity>) convenientRepository.findByNameContaining(str);
        return promotions;
    }

}
