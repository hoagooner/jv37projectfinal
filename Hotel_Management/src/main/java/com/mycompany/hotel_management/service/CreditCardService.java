/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.service;

import com.mycompany.hotel_management.entities.BookingEntity;
import com.mycompany.hotel_management.entities.CreditCardEntity;
import com.mycompany.hotel_management.entities.PaymentEntity;
import com.mycompany.hotel_management.enums.PaymentMethod;
import com.mycompany.hotel_management.enums.PaymentStatus;
import com.mycompany.hotel_management.repository.CreditCardRepository;
import com.mycompany.hotel_management.repository.PaymentRepository;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author abcdef
 */
@Service
public class CreditCardService {

    @Autowired
    private CreditCardRepository creditCardRepository;

    public CreditCardEntity findByCardNumber(String cardNumber) {
        return creditCardRepository.findByCardNumber(cardNumber);
    }

    public void save(CreditCardEntity creditCard) {
        creditCardRepository.save(creditCard);
    }

}
