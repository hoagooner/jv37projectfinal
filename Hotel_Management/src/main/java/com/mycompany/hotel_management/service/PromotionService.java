/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.service;

import com.mycompany.hotel_management.entities.PromotionEntity;
import com.mycompany.hotel_management.entities.RoomTypeEntity;
import com.mycompany.hotel_management.repository.PromotionRepository;
import java.util.List;
import java.util.Optional;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author abcdef
 */
@Service

public class PromotionService {

    @Autowired
    private PromotionRepository promotionRepository;

    public void save(PromotionEntity promo) {
        promotionRepository.save(promo);
    }

    @Transactional
    public List<PromotionEntity> getPromotions() {
        return promotionRepository.findAll();
    }

    @Transactional
    public PromotionEntity findById(int id) {
        PromotionEntity promo = promotionRepository.findById(id).get();
        Hibernate.initialize(promo.getImages());
        for(RoomTypeEntity roomType : promo.getRoomTypes()){
            Hibernate.initialize(roomType.getImages());
        }
        return promo;
    }

    @Transactional
    public List<PromotionEntity> findByName(String str) {
        return promotionRepository.findByName(str);
    }

}
