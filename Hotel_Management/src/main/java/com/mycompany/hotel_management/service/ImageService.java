/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.service;

import com.mycompany.hotel_management.entities.ImageEntity;
import com.mycompany.hotel_management.repository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author abcdef
 */
@Service
public class ImageService {
    
    @Autowired
    private ImageRepository imageRepository;
    
    public void save(ImageEntity image){
        imageRepository.save(image);
    }
    
    public void delete(int id)
    {
        imageRepository.deleteById(id);
    }
}
