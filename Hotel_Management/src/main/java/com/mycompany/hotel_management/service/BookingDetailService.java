/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.service;

import com.mycompany.hotel_management.entities.BookingDetailEntity;
import com.mycompany.hotel_management.entities.BookingEntity;
import com.mycompany.hotel_management.entities.RoomEntity;
import com.mycompany.hotel_management.entities.RoomTypeEntity;
import com.mycompany.hotel_management.entities.ServiceEntity;
import com.mycompany.hotel_management.repository.BookingDetailRepository;
import java.util.List;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author OS
 */
@Service
public class BookingDetailService {

    @Autowired
    private RoomService roomService;

    @Autowired
    private PromotionService promotionService;

    @Autowired
    private ServiceService serviceService;

    @Autowired
    private BookingDetailRepository bookingDetailRepository;

    @Transactional
    public List<BookingDetailEntity> getBookingDetail() {
        List<BookingDetailEntity> bookingDetails = (List<BookingDetailEntity>) bookingDetailRepository.findAll();
        for (BookingDetailEntity bookingDetail : bookingDetails) {
            Hibernate.initialize(bookingDetail.getServices());
            Hibernate.initialize(bookingDetail.getDiscount());
            Hibernate.initialize(bookingDetail.getRoom());
            Hibernate.initialize(bookingDetail.getRoom());
        }
        return bookingDetails;

    }

    @Transactional
    public BookingDetailEntity findById(int id) {
        BookingDetailEntity booking = bookingDetailRepository.findById(id).get();
        return booking;
    }
    
    public void save(BookingDetailEntity bookingDetail, BookingEntity booking) {
        bookingDetail.setBooking(booking);
        bookingDetailRepository.save(bookingDetail);
    }
    
    public void save(BookingDetailEntity bookingDetail) {
        bookingDetailRepository.save(bookingDetail);
    }

//    public void save(BookingEntity booking, RoomEntity room) {
//        BookingDetailEntity bookingDetail = new BookingDetailEntity();
//        bookingDetail.setRoom(room);
//        bookingDetail.setBooking(booking);
//        bookingDetail.setPrice(room.getPrice());
//
//        bookingDetailRepository.save(bookingDetail);
//    }
}
