/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.service;

import com.mycompany.hotel_management.entities.BookingEntity;
import com.mycompany.hotel_management.entities.CreditCardEntity;
import com.mycompany.hotel_management.entities.PaymentEntity;
import com.mycompany.hotel_management.enums.PaymentMethod;
import com.mycompany.hotel_management.enums.PaymentStatus;
import com.mycompany.hotel_management.repository.CreditCardRepository;
import com.mycompany.hotel_management.repository.PaymentRepository;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author abcdef
 */
@Service
public class PaymentService {

    @Autowired
    private PaymentRepository paymentRepository;
    
    @Autowired
    private CreditCardRepository creditCardRepository;

    public void save(PaymentEntity payment) {
        paymentRepository.save(payment);
    }

    @Transactional(rollbackFor = Exception.class)
    public void transferMoney(CreditCardEntity guestCardEntity, BookingEntity booking) throws Exception {
        CreditCardEntity hotelCreditCard = creditCardRepository.findByCardNumber("1234567890");
        

        if("1234567890".equals(guestCardEntity.getCardNumber())){

        }else{
        guestCardEntity.setBalance(guestCardEntity.getBalance() - booking.getTotalPrice());
        if (guestCardEntity.getBalance() < 0) {
            throw new Exception("amount greater than balance");
        }
        hotelCreditCard.setBalance(hotelCreditCard.getBalance() + booking.getTotalPrice());
        }

        PaymentEntity payment = new PaymentEntity();
        payment.setPaymentDate(new Date());
        payment.setBooking(booking);
        payment.setAmount(booking.getTotalPrice());
        payment.setMethod(PaymentMethod.CREDIT_CARD);
        payment.setStatus(PaymentStatus.PAID);
        payment.setCreditCard(guestCardEntity);
        Set<PaymentEntity> payments = new HashSet<>();
        payments.add(payment);
        booking.setPayments(payments);
        
        
        creditCardRepository.save(guestCardEntity);
        creditCardRepository.save(hotelCreditCard);
        
    }
}
