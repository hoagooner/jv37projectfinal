/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.service;

import com.mycompany.hotel_management.entities.ServiceEntity;
import com.mycompany.hotel_management.repository.ServiceRepository;
import java.util.List;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author OS
 */
@Service

public class ServiceService {

    @Autowired
    private ServiceRepository serviceRepository;

    public void save(ServiceEntity promo) {
        serviceRepository.save(promo);
    }

    @Transactional
    public List<ServiceEntity> getServices() {
        List<ServiceEntity> services = (List<ServiceEntity>) serviceRepository.findAll();
        for (ServiceEntity sevice : services) {
            Hibernate.initialize(sevice.getImages());
        }
        return services;
    }

    @Transactional
    public ServiceEntity findById(int id) {
        ServiceEntity service = serviceRepository.findById(id).get();
        Hibernate.initialize(service.getImages());
        return service;
    }
    
    @Transactional
    public List<ServiceEntity> findByName(String name) {
        List<ServiceEntity> services = (List<ServiceEntity>) serviceRepository.findByName(name);
        for (ServiceEntity sevice : services) {
            Hibernate.initialize(sevice.getImages());
        }
        return services;
    }
}
