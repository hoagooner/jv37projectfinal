/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.service;

import com.mycompany.hotel_management.entities.ConvenientEntity;
import com.mycompany.hotel_management.entities.PromotionEntity;
import com.mycompany.hotel_management.entities.RoomEntity;
import com.mycompany.hotel_management.entities.RoomTypeEntity;
import com.mycompany.hotel_management.enums.CommonStatus;
import com.mycompany.hotel_management.enums.RoomType;
import com.mycompany.hotel_management.repository.RoomTypeRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author abcdef
 */
@Repository
public class RoomTypeService {

    @Autowired
    private RoomTypeRepository roomTypeRepository;

    @Autowired
    private ConvenientService convenientService;

    @Autowired
    private PromotionService promotionService;

    public void save(RoomTypeEntity roomType) {
        roomTypeRepository.save(roomType);
    }

 

    @Transactional
    public List<RoomTypeEntity> getRoom_Promotion(Date endDate, Date startDate) {
        List<RoomTypeEntity> roomTypes = roomTypeRepository.getRoomType_Promo(endDate, startDate);
        for (RoomTypeEntity roomTypeEntity : roomTypes) {
            Hibernate.initialize(roomTypeEntity.getImages());
            Hibernate.initialize(roomTypeEntity.getPromotions());
        }
        return roomTypes;
    }

    public List<Object[]> getAvailableRooms(int person, Date checkIn, Date checkOut) {
        return roomTypeRepository.getAvailableRooms(person, checkIn, checkOut);
    }

    public List<RoomType> getRoomTypesExists() {
        List<RoomTypeEntity> roomTypes = (List<RoomTypeEntity>) roomTypeRepository.findByStatus(CommonStatus.ACTIVE);
        List<RoomType> RoomTypesExists = new ArrayList<>();
        for (RoomTypeEntity roomType : roomTypes) {
            RoomTypesExists.add(roomType.getName());
        }
        return RoomTypesExists;
    }

    public List<RoomTypeEntity> getRoomTypes() {
        return (List<RoomTypeEntity>) roomTypeRepository.findAllQuery();
    }

    public List<RoomTypeEntity> getRoomType_Room() {
        return roomTypeRepository.getRoomType_Room();
    }

    @Transactional
    public RoomTypeEntity findById(int id) {
        RoomTypeEntity roomType = roomTypeRepository.findById(id);
        Set<ConvenientEntity> convenients = roomType.getConvenients();
        for (ConvenientEntity convenient : convenients) {
            convenient = convenientService.findById(convenient.getId());
        }
        Hibernate.initialize(roomType.getConvenients());
        Hibernate.initialize(roomType.getPromotions());
        return roomType;
    }
    
    public RoomTypeEntity findByIdSearch(int id) {
        return roomTypeRepository.findById(id);
    }

    public List<RoomTypeEntity> findByName(RoomType name) {
        return roomTypeRepository.findByName(name);
    }

    @Transactional
    public List<RoomTypeEntity> getActiveRoomTypes() {
        List<RoomTypeEntity> roomTypes = roomTypeRepository.getRoomTypesByStatus(CommonStatus.ACTIVE);
        for (RoomTypeEntity roomTypeEntity : roomTypes) {
            Hibernate.initialize(roomTypeEntity.getPromotions());
        }
        return roomTypes;

    }
}
