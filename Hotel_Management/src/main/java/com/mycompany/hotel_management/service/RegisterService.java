/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.service;

import com.mycompany.hotel_management.entities.UserEntity;
import com.mycompany.hotel_management.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author OS
 */
@Service
public class RegisterService {
    
    @Autowired
    private UserRepository userRepository;
    
    public void save(UserEntity categoryEntity){
        userRepository.save(categoryEntity);
    }
}
