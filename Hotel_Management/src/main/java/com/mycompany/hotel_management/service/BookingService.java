/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.service;

import com.mycompany.hotel_management.entities.BookingDetailEntity;
import com.mycompany.hotel_management.entities.BookingEntity;
import com.mycompany.hotel_management.enums.BookingStatus;
import com.mycompany.hotel_management.repository.BookingRepository;
import java.util.Date;
import java.util.List;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author OS
 */
@Service
public class BookingService {

    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private BookingDetailService bookindetailService;

    @Autowired
    private RoomService roomService;

    public void save(BookingEntity booking) {
        bookingRepository.save(booking);
    }

    public void delete(int id) {
        bookingRepository.deleteById(id);
    }

    public int getBookingQuantity() {
        return bookingRepository.getBookingQuantity();
    }

    public int getGuestQuantity() {
        return bookingRepository.getGuestQuantity();
    }

    @Transactional
    public BookingEntity findByEmailAndBookingNumber(String email, String bookingNumber) {
        BookingEntity bookingEntity = bookingRepository.findByEmailAndBookingNumber(email, bookingNumber);
        if (bookingEntity != null) {
            return findById(bookingEntity.getId());
        }
        return new BookingEntity();
    }

    @Transactional
    public BookingEntity findByBookingNumber(String bookingNumber) {
        BookingEntity bookingEntity = bookingRepository.findByBookingNumber(bookingNumber);
        if (bookingEntity != null) {
            return findById(bookingEntity.getId());
        }
        return new BookingEntity();
    }

    @Transactional
    public List<BookingEntity> findAll() {
        List<BookingEntity> reservations = (List<BookingEntity>) bookingRepository.findAll();
        for (BookingEntity reservation : reservations) {
            Hibernate.initialize(reservation.getBookingDetails());
        }
        return reservations;
    }

    @Transactional
    public BookingEntity findById(int id) {
        BookingEntity booking = bookingRepository.findById(id).get();
        Hibernate.initialize(booking.getBookingDetails());
        Hibernate.initialize(booking.getPayments());
        for (BookingDetailEntity bookingDetail : booking.getBookingDetails()) {
            Hibernate.initialize(bookingDetail.getServices());
            bookingDetail.setRoom(roomService.findById(bookingDetail.getRoom().getId()));
        }
        return booking;
    }

    public List<BookingEntity> searchOrdersByDate(Date date1, Date date2) {
        return bookingRepository.findOrderByDate(date1, date2);
    }
}
