/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hotel_management.service;

import com.mycompany.hotel_management.entities.PromotionEntity;
import com.mycompany.hotel_management.entities.RoomEntity;
import com.mycompany.hotel_management.entities.RoomTypeEntity;
import com.mycompany.hotel_management.enums.RoomType;
import com.mycompany.hotel_management.repository.RoomRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author abcdef
 */
@Service
public class RoomService {

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private RoomTypeService roomTypeService;

    public void save(RoomEntity room) {
        roomRepository.save(room);
    }
    
    public List<RoomEntity> searchRooms(int person, Date checkin, Date checkout){
        return roomRepository.getAvailableRooms(person, checkin, checkout);
    }
    

    public List<RoomEntity> getRooms() {
        List<RoomEntity> rooms = (List<RoomEntity>) roomRepository.findAll();
        for (RoomEntity room : rooms) {
            RoomTypeEntity roomType = roomTypeService.findById(room.getRoomType().getId());
            room.setRoomType(roomType);
        }
        return rooms;
    }
    
    public int getRoomQuantity(){
        return roomRepository.getRoomQuantity();
    }

    public List<RoomEntity> getBookedRoomToDay(){
        return roomRepository.getBookedRoomToDay();
    }

    public RoomEntity findById(int id) {
        RoomEntity room = roomRepository.findById(id);
        if (room.getId() > 0) {
            RoomTypeEntity roomType = roomTypeService.findById(room.getRoomType().getId());
            room.setRoomType(roomType);
            return room;
        }
        return new RoomEntity();
    }
    public List<RoomEntity> findByRoomNumber(int roomNumber) {
        List<RoomEntity> rooms = roomRepository.findByRoomNumber(roomNumber);
        for (RoomEntity room : rooms) {
            RoomTypeEntity roomType = roomTypeService.findById(room.getRoomType().getId());
            room.setRoomType(roomType);
        }
        return rooms;
    }

    public List<List<RoomEntity>> getRoomsByRoomType() {
        List<List<RoomEntity>> roomsByRoomType = new ArrayList<>();
        List<RoomEntity> rooms = (List<RoomEntity>) roomRepository.findAll();
        List<RoomTypeEntity> roomTypes = roomTypeService.getActiveRoomTypes();
        for (RoomTypeEntity roomType : roomTypes) {
            List<RoomEntity> roomByRoomType = new ArrayList<>();
            for (RoomEntity room : rooms) {
                if(room.getRoomType().getName().equals(roomType.getName())){
                    roomByRoomType.add(room);
                }
            }
            roomsByRoomType.add(roomByRoomType);
        }
        return roomsByRoomType;
    }
    
  
}
