<%-- 
    Document   : login
    Created on : Jun 20, 2019, 8:17:26 PM
    Author     : AnhLe
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- Font Icon -->
        <link rel="stylesheet" href="resources/register/fonts/material-icon/css/material-design-iconic-font.min.css">

        <!-- Main css -->

        <link rel="stylesheet" href="resources/register/css/style.css">
    </head>
    <body>
        <!-- /login?error=true -->

        <form action="<c:url value="j_spring_security_check"/>" method="post">
            <!--            <table>
                            <tr>
                                <th></th>
                                <th></th>
                            </tr>
                            <tr>
                                <td>User Name</td>
                                <td>
                                    <input name="username" type="email"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Password</td>
                                <td>
                                    <input name="password" type="password"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <input type="submit" value="login"/>
                                </td>
                            </tr>
                        </table>
                        <input type="hidden" name="$ {_csrf.parameterName}" value="$ {_csrf.token}"/>-->
            <section class="sign-in">
                <div class="container">
                    <div class="signin-content">
                        <div class="signin-image">
                            <figure><img src="resources/register/images/signin-image.jpg" alt="sing up image"></figure>
                            <a href="${pageContext.request.contextPath}/register" class="signup-image-link">Create an account</a>
                        </div>

                        <div class="signin-form">
                            <h2 class="form-title">Sign up</h2>
                            <form method="POST" class="register-form" id="login-form">
                                <div class="form-group">
                                    <label for="your_name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                                    <input name="username" type="email"/>
                                </div>
                                <div class="form-group">
                                    <label for="your_pass"><i class="zmdi zmdi-lock"></i></label>
                                    <input name="password" type="password"/>
                                </div>
                                <c:if test="${message != null && message != ''}">
                                    <p style="color: red">${message}</p>
                                </c:if>
                                <div class="form-group">
                                    <input type="checkbox" name="remember-me" id="remember-me" class="agree-term" />
                                    <label for="remember-me" class="label-agree-term"><span><span></span></span>Remember me</label>
                                </div>
                                <div class="form-group form-button">
                                    <input type="submit" value="login"/>
                                </div>
                            </form>
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/> 
                            <div class="social-login">
                                <span class="social-label">Or login with</span>
                                <ul class="socials">
                                    <li><a href="#"><i class="display-flex-center zmdi zmdi-facebook"></i></a></li>
                                    <li><a href="#"><i class="display-flex-center zmdi zmdi-twitter"></i></a></li>
                                    <li><a href="#"><i class="display-flex-center zmdi zmdi-google"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>
        <script src="resources/register/vendor/jquery/jquery.min.js"></script>
        <script src="resources/register/js/main.js"></script>
    </body>
</html>
