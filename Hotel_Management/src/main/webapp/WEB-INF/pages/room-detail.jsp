<%-- 
    Document   : home2
    Created on : Nov 18, 2020, 8:02:05 PM
    Author     : ASUS
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Hiroto Template">
        <meta name="keywords" content="Hiroto, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Hiroto</title>

        <jsp:include page="include/css-header.jsp" />
    </head>
    <body>
        <jsp:include page="include/nav-header.jsp" />
        <!--list room-->
        <div class="room-details-slider">
            <div class="container">
                <div class="room__details__pic__slider owl-carousel">
                    <c:forEach var="image" items="${roomType.images}">
                        <div class="room__details__pic__slider__item" style='background-image: url(<c:url value='/upload/images/${image.name}' />)'></div>
                    </c:forEach>
                </div>
            </div>
        </div>
        <!-- Room Details Slider End -->

        <!-- Rooms Details Section Begin -->
        <section class="room-details spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="room__details__content">
                            <div class="room__details__rating">
                                <div class="room__details__hotel">
                                    <span>Hotel</span>
                                    <div class="room__details__hotel__rating">
                                        <span class="icon_star"></span>
                                        <span class="icon_star"></span>
                                        <span class="icon_star"></span>
                                        <span class="icon_star"></span>
                                        <span class="icon_star-half_alt"></span>
                                    </div>
                                    <span class="review">(1000 Reviews)</span>
                                </div>
                            </div>
                            <div class="room__details__title">
                                <h2>${roomType.name} ROOM</h2>
                                <h2>$ ${roomType.price}</h2>
                                <a href="#" class="primary-btn">Booking Now</a>
                            </div>
                            <div class="room__details__desc">
                                <h2>Description:</h2>
                                ${roomType.description}
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="room__details__more__facilities">
                                        <h2>${roomType.name} ROOM</h2>
                                        <div class="row">
                                            <div class="room__details__more__facilities__item" style="margin-left: 20px">
                                                <h5>Maximum Occupancy:  ${roomType.occupancy} Guests</h5>
                                                <h5>area: ${roomType.size}m <sup>2</sup></h5>
                                                <h5>Number Of Bed: ${roomType.numberOfBed} bed </h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="room__details__more__facilities">
                                        <h2>Convenient</h2>
                                        <div class="row">
                                            <c:forEach items="${roomType.convenients}" var="convenient">
                                                <c:if test="${convenient.status eq 'ACTIVE'}">
                                                <div class="col-lg-6">
                                                    <div class="room__details__more__facilities__item">
                                                        <c:if test="${!empty convenient.images}">
                                                            <div class="icon"><img src="<c:url value='/upload/images/${convenient.images.iterator().next().name}' />"
                                                                                   alt="">
                                                            </div>
                                                        </c:if>
                                                        <h6>${convenient.name}</h6>
                                                    </div>
                                                </div>
                                                </c:if>
                                            </c:forEach>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="room__details__more__facilities">
                                        <h2>Services</h2>
                                        <div class="row">
                                            <c:forEach items="${services}" var="service">
                                                <c:if test="${service.status eq 'ACTIVE'}">
                                                <div class="col-lg-6">
                                                    <div class="room__details__more__facilities__item">
                                                        <c:if test="${!empty service.images}">
                                                            <div class="icon"><img src="<c:url value='/upload/images/${service.images.iterator().next().name}' />"
                                                                                   alt="">
                                                            </div>
                                                        </c:if>
                                                        
                                                            <h6>${service.name}</h6>
                                                        
                                                        
                                                    </div>
                                                </div>
                                                            </c:if>
                                            </c:forEach>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <jsp:include page="include/footer.jsp" />
    <jsp:include page="include/js-page.jsp" />
</body>
</html>
