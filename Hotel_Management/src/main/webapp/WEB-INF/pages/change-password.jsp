<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="mvc"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
<%@taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
    <title>Sign Up Form by Colorlib</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="resources/register/fonts/material-icon/css/material-design-iconic-font.min.css">

    <!-- Main css -->
    
    <link rel="stylesheet" href="resources/register/css/style.css">
</head>
<body>

    <div class="main">

        <!-- Sign up form -->
        <section class="signup">
            <div class="container">
                <div class="signup-content">
                    <div class="signup-form">
                        <h2 class="form-title">CHANGE PASSWORD</h2>
                        <form action="${pageContext.request.contextPath}/change-password" id="loginForm" modelAttribute="userEntity">
                                <div class="form-group">
                                    <label class="control-label" for="username">Username</label>
                                    <input type="text" style="background-color: #9acfea" title="Please enter you username" required="" 
                                           value="${userEntity.email}" name="email" id="username" class="form-control" readonly="">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="password">Password</label>
                                    <input type="password" title="Please enter your password" placeholder="******" required="" name="password" id="password" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="password">Confirm Password</label>
                                    <input type="password" title="Please enter your password" 
                                           placeholder="******" required="" name="confirm_password" id="confirm_password" class="form-control"><span id='messagePass'></span>
                                </div>
                                <input type="text" value="${userEntity.id}" name="id" hidden="">
                                <input type="text" value="${userEntity.fullName}" name="fullName" hidden="">
                                <input type="text" value="${userEntity.uuid}" name="uuid" hidden="">
                                <c:forEach var="role" items="${roles}">
                                    <% String check = ""; %>
                                    <c:forEach items="${userEntity.getUserRoles()}" var="roleUser"> 
                                        <c:if test="${roleUser.id == role.id}">
                                            <% check = "checked";%>
                                        </c:if>
                                    </c:forEach>
                                    <div>
                                        <div class="checkbox">
                                            <label hidden="">
                                                <input type="checkbox" <%=check%> name="role" value="${role.id}" hidden="true">
                                                ${role.getRole()}
                                            </label>
                                        </div>
                                    </div>     
                                </c:forEach>
                                <button type="submit" class="btn btn-success btn-block loginbtn">Submit</button>
                            </form>
                    </div>
                </div>
            </div>
        </section>

        <!-- Sing in  Form -->
        

    </div>

    <!-- JS -->
    <script src="resources/register/vendor/jquery/jquery.min.js"></script>
    <script src="resources/register/js/main.js"></script>


        <script>
            $('#password, #confirm_password').on('keyup', function () {
                if ($('#password').val() == $('#confirm_password').val()) {
                    $('#messagePass').html('Matching').css('color', 'green');
                } else
                    $('#messagePass').html('Not Matching').css('color', 'red');
            });
        </script>

    <!-- JS -->
    <script src="resources/register/js-crat.jsp"></script>

    </body>
</html>
