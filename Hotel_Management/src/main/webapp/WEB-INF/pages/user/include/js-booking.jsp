<div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214"/></svg></div>

    <script src="resources/js_b/jquery-3.2.1.min.js"></script>
    <script src="resources/js_b/jquery-migrate-3.0.0.js"></script>
    <script src="resources/js_b/popper.min.js"></script>
    <script src="resources/js_b/bootstrap.min.js"></script>
    <script src="resources/js_b/owl.carousel.min.js"></script>
    <script src="resources/js_b/jquery.waypoints.min.js"></script>
    <script src="resources/js_b/jquery.stellar.min.js"></script>

    <script src="resources/js_b/jquery.magnific-popup.min.js"></script>
    <script src="resources/js_b/magnific-popup-options.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/resources/js_b/bootstrap-datepicker.min.js"></script>

    <script>
      
      $('#arrival_date, #departure_date').datepicker({});

    </script>

    

    <script src="resources/js_b/main.js"></script>