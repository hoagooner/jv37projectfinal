<%@page import="java.util.Date"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<form action="${pageContext.request.contextPath}/search" class="filter__form">
    <div class="row">
        <div class="col-lg-4" style="text-align: center">
            <div class="filter__form__item">
                <p>Check In</p>
                <div class="filter__form__datepicker">
                    <span class="icon_calendar"></span>
                    <input type="text" id="checkIn" name="checkIn" 
                           value="${checkIn}" class="datepicker_pop" placeholder="Check In" autocomplete="off"/>
                    <i class="arrow_carrot-down"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-4" style="text-align: center">
            <div class="filter__form__item">
                <p>Check Out</p>
                <div class="filter__form__datepicker">
                    <span class="icon_calendar"></span>
                    <input type="text" id="checkOut" name="checkOut"
                     value="${checkOut}" class="datepicker_pop" placeholder="Check Out" autocomplete="off"/>
                    <i class="arrow_carrot-down"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-4" style="text-align: center">
            <div class="filter__form__item filter__form__item--select">
                <p>Person</p>
                <div class="filter__form__select">
                    <span class="icon_group"></span>
                    <select name="person">
                        <c:if test="${person != null}">
                            <option value="${person}" selected>${person} Person</option>
                        </c:if>
                        <%
                            int person = session.getAttribute("person") != null  ? (int)session.getAttribute("person") : 0;
                            for (int i = 1; i <= 4; i++) {
                                if (i != person) {
                        %>
                        <option value="<%=i%>"><%=i%> Person</option>
                        <%
                                }
                            }
                        %>

                    </select>
                </div>
            </div>
        </div>
        <button type="submit">SEARCH</button>
    </div>
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>

        <script type="text/javascript">
            $(function () {

                $('#checkIn').datepicker({
                    dateFormat: 'dd-mm-yy',
                    altField: '#thealtdate',
                    altFormat: 'yy-mm-dd'
                });

                $('#checkOut').datepicker({
                    dateFormat: 'dd-mm-yy',
                    altField: '#thealtdate',
                    altFormat: 'yy-mm-dd'
                });

                $('#checkIn').on('change', function () {
                    var checkIn = $('#checkIn').val();
                    var cdate = new Date();
                    var diff = new Date(new Date(checkIn) - new Date(cdate));
                    var days = diff / 1000 / 60 / 60 / 24;
                    if (days + 1 < 0) {
                        alert("Sorry, you must choose a start date greater than the current date!");
                        $('#checkIn').val('');
                    } else {

                    }
                    return false;
                });

                $('#checkOut').on('change', function () {
                    var checkIn = $('#checkIn').val();
                    var checkOut = $('#checkOut').val();
                    var diff = new Date(new Date(checkOut) - new Date(checkIn));
                    var days = diff / 1000 / 60 / 60 / 24;
                    if (days < 0 || days > 31) {
                        alert("Sorry! You must pick an end date that is greater!");
                        $('#checkOut').val('');
                    } else {
                        alert(checkIn + "-" + checkOut + "(" + days + "night" + ")");
                    }
                    return false;
                });

            });
        </script>
</form>
