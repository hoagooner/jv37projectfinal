<%-- 
    Document   : home2
    Created on : Nov 18, 2020, 8:02:05 PM
    Author     : ASUS
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Hiroto Template">
        <meta name="keywords" content="Hiroto, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Hiroto</title>
        <jsp:include page="include/css-header.jsp" />
    </head>
    <body>
        <jsp:include page="include/nav-header.jsp" />
        <!--form booking-->
        <section class="hero spad" style='background-image: url("resources/img/hero.jpg");'>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="hero__text">
                            <h5>WELCOME HIROTO</h5>
                            <h2>Experience the greatest for you holidays.</h2>
                        </div>
                        <jsp:include page="include/search-form.jsp"/>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <c:if test="${message != null}">
                            <div class="alert alert-danger">${message}</div>
                        </c:if>
                    </div>
                </div>
            </div>
        </section>

        <!-- Home About Section Begin -->
        <section class="home-about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="home__about__text">
                            <div class="section-title">
                                <h5>ABOUT US</h5>
                                <h2>Welcome Hiroto Hotel In Street L’Abreuvoir</h2>
                            </div>
                            <p class="first-para">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut
                                fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                            <p class="last-para">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                doloremque.</p>
                            <img src="img/home-about/sign.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="home__about__pic">
                            <img src="img/home-about/home-about.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Home About Section End -->
        <!-- Home Room Section Begin -->
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h5>OUR ROOM</h5>
                        <h2>Explore Our Hotel</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <c:forEach var="roomType" items="${roomTypes}">
                    <div class="col-md-4 mb-4">
                        <div class="media d-block room mb-0">
                            <figure>
                                <c:if test="${!empty roomType.images}">
                                    <img class="img-fluid" src="<c:url value='/upload/images/${roomType.images.iterator().next().name}' />"
                                         alt="image" />
                                </c:if>

                                <div class="overlap-text">
                                    <span>
                                        <span class="ion-ios-star"></span>
                                        <span class="ion-ios-star"></span>
                                        <span class="ion-ios-star"></span>
                                    </span>
                                </div>
                            </figure>
                            <div class="media-body">
                                <a href="${pageContext.request.contextPath}/room-detail/${roomType.id}">
                                    <h2>${roomType.name} ROOM</a></h2>
                                <ul class="room-specs">
                                    <li><span class="ion-ios-people-outline"></span> ${roomType.occupancy} Guests</li>
                                    <li><span class="ion-ios-crop"></span> ${roomType.size}m <sup>2</sup></li>
                                    <li>
                                        <i class="fas fa-bed"></i>
                                        ${roomType.numberOfBed} bed
                                    </li>
                                </ul>
                                <p>${roomType.description}</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>


        <div class="container">
            <div class="home__explore">
                <div class="row">
                    <div class="col-lg-9 col-md-8">
                        <h3>Planning your next trip? Save up to 25% on your hotel</h3>
                    </div>
                    <div class="col-lg-3 col-md-4 text-center">
                        <a href="#" class="primary-btn">Explorer More</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Home Room Section End -->


        <!-- Services Section Begin -->
        <section class="services spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="services__item">
                            <img src="resources/img/services/services-1.png" alt="">
                            <h4>Free Wi-Fi</h4>
                            <p>The massive investment in a hotel or resort requires constant reviews and control in order to
                                make it a successful investment.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="services__item">
                            <img src="resources/img/services/services-1.png" alt="">
                            <h4>Premium Pool</h4>
                            <p>Choose from 4 unique ready made concepts, let us help you create the concept perfect for you
                                or let HCA.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="services__item">
                            <img src="resources/img/services/services-1.png" alt="">
                            <h4>Coffee Maker</h4>
                            <p>HCA's Owner's Representation is taking care of just these important factors, may it be
                                through regular site visits and spot checks.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Services Section End -->
        <jsp:include page="include/footer.jsp" />
        <jsp:include page="include/js-page.jsp" />
    </body>
</html>
