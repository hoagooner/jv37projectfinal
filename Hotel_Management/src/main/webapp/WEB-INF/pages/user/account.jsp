<%-- 
    Document   : account
    Created on : Dec 10, 2020, 9:49:48 PM
    Author     : nguye
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="mvc"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Account Page</title>

        <jsp:include  page="include/css.jsp"/>
    </head>
    <body>

        <!-- BREADCRUMB -->
        <div id="breadcrumb" class="section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="breadcrumb-header">Account</h3>
                        <ul class="breadcrumb-tree">
                            <li><a href="<c:url value="/home" />">Home</a></li>
                            <li class="active">Account</li>
                        </ul>
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /BREADCRUMB -->
        <!-- NEWSLETTER -->
        <div id="newsletter" class="section">
            <!-- container -->
            <div class="container">
                <mvc:form action="${pageContext.request.contextPath}/result" method="post"
                          modelAttribute="userEntity" enctype="multipart/form-data">
                    <div  class="caption">
                        <p>Update Customer Info: ${userEntity.email}</p>
                        <div class="form-group">
                            <input class="input" type="text" name="fullName" placeholder="your name"  value="${userEntity.fullName}" required>
                        </div>

                        <div class="form-group">
                            <input  class="input" id="password" type="hidden" name="email"  value="${userEntity.email}" required>
                        </div>
                        <div class="form-group">
                            <input  class="input" id="password" type="hidden" name="uuid"  value="${userEntity.uuid}" required>
                        </div>
                        <div class="form-group">
                            <input  class="input" id="password" type="hidden" name="status"  value="${userEntity.status}" required>
                        </div>

                        <div class="form-group">
                            <input  class="input" id="password" name="password" placeholder="password" value="${userEntity.password}" required>
                        </div><!--
-->                        <div class="form-group">
                            <input class="input" id="confirm_password" type="password" name="confirm_password" placeholder="Confirm password"  value="${userEntity.password}"> <span id='messagePass'></span>
                        </div>
                        <div class="form-group">
                            <input class="input" type="text" name="address" placeholder="Address" value="${userEntity.address}" required>
                        </div>
                        <div class="form-group">
                            <input class="input" type="date" name="birhtDay" placeholder="Birthday" value="${userEntity.birhtDay}" required>
                        </div>
                        <div class="form-group">
                            <input class="input" type="text" name="phoneNumber" placeholder="Phone Number" value="${userEntity.phoneNumber}" required>
                        </div>
                        <!--<button  onclick="window.location.href = '<c:url value="/payment"/> '">Payment on cash</button>-->
                        <!--<input  name="delivery" type="submit"  style="width: 100%" class="primary-btn order-submit" value="Save"/>-->
                        <c:if test="${action == 'update'}">
                            <input type="hidden" value="update" name="action">
                            <input type="hidden" value="${userEntity.id}" name="id">
                            <button style="width: 100%" class="primary-btn order-submit" type="submit">
                                Update Account     
                            </button>
                        </c:if>
                    </div>
                </mvc:form>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /NEWSLETTER -->

        <jsp:include page="include/footer.jsp"/>
        <jsp:include page="include/js-page.jsp"/>
        <jsp:include page="include/js-crat.jsp"/>
        <script>
            $('#password, #confirm_password').on('keyup', function () {
                if ($('#password').val() == $('#confirm_password').val()) {
                    $('#messagePass').html('Matching').css('color', 'green');
                } else
                    $('#messagePass').html('Not Matching').css('color', 'red');
            });
        </script>

    </body>
</html>
