<%-- 
    Document   : home2
    Created on : Nov 18, 2020, 8:02:05 PM
    Author     : ASUS
--%>

<%@page import="java.util.Date"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="mvc"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Hiroto Template">
        <meta name="keywords" content="Hiroto, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <script src="<c:url value="/resources/js/jquery-3.3.1.min.js"/>"></script>

        <title>Result Room</title>
        <style>
            .hidden{ 
                height:0;width:0;overflow:hidden; 
            }
        </style>
        <jsp:include page="include/css-header.jsp" />
    </head>
    <body>
        <jsp:include page="include/nav-header.jsp" />
        <!--list room-->
        <section class="hero spad" style='background-image: url("resources/img/breadcrumb-bg.jpg")'>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <jsp:include page="include/search-form.jsp"/>
                    </div>
                </div>
            </div>
        </section>
        <h3 style="color: red">${mess}</h3>
        <section class="rooms spad">
            <div class="container">
                <c:forEach var="roomType" items="${roomTypes}">
                    <c:if test="${roomType.status eq 'ACTIVE'}">
                    <c:if test="${!empty roomType.rooms}">
                        <mvc:form action="${pageContext.request.contextPath}/your-booking"
                                  method="post" modelAttribute="bookingDetail">
                            <div class="row">
                                <div class="col-lg-6 p-0order-lg-1 order-md-1">
                                    <div class="room__pic__slider owl-carousel">
                                        <c:if test="${!empty roomType.images}">
                                            <c:forEach var="image" items="${roomType.images}">
                                                <div class="room__pic__item" style='background-image: url(<c:url value='/upload/images/${image.name}' />)'></div>
                                            </c:forEach>
                                        </c:if>
                                    </div>
                                </div>
                                <div class="col-lg-6 p-0  col-md-6 order-lg-2 order-md-2">
                                    <div class="room__text"  style="margin-left: 50px">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <h3>${roomType.name} ROOM</h3>
                                            </div>
                                            <div class="col-lg-6" >
                                                <c:if test="${roomType.resultDiscount() != 0}">
                                                    <div style="float: right">
                                                        <button type="button" class="btn btn-outline-danger btn-xs" >
                                                            ${roomType.resultDiscount()}%
                                                        </button>
                                                        <h5 style="text-decoration: line-through;color:red; display: flex">
                                                            ${roomType.price}<span>/day</span>
                                                        </h5>
                                                    </div>  
                                                </c:if>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <h2>${roomType.salePrice()}<span>VND/day</span></h2>
                                        </div>

                                        <%--
                                         <c:forEach var="promo" items="${roomType.promotions}">
                                            <fmt:formatDate var="startDate" value="${promo.startDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
                                            <fmt:formatDate var="endDate" value="${promo.endDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
                                            <c:set var="today" value="<%=new java.util.Date()%>"/>
                                            <fmt:formatDate var="now" value="${today}" pattern="yyyy-MM-dd HH:mm:ss"/>
                                            <c:if test="${startDate lt now && endDate gt now}">
                                                <button type="button" class="btn btn-outline-danger btn-xs">
                                                    ${promo.discount}%
                                                </button>
                                            </c:if>
                                        </c:forEach>--%>

                                        <div class="col-md-2" 
                                             style="border: 1px solid #E9AD28;float: right;text-align: center">
                                            <h5 style="font-size:30px"> ${roomType.rooms.size()}</h5>${roomType.rooms.size() == 1 ? "room":"rooms"} available
                                        </div>


                                        <ul>
                                            <li><span>Size:</span>${roomType.size} m<sup>2</sup></li>
                                            <li><span>Capacity:</span>Max person ${roomType.occupancy}</li>
                                            <li><span>Bed:</span>${roomType.numberOfBed}</li>
                                            <li><span>Convenient</span>
                                                <c:forEach var="convenient" items="${roomType.convenients}">
                                                    ${convenient.name} 
                                                </c:forEach>
                                            </li>
                                        </ul>
                                        <a href="${pageContext.request.contextPath}/room-detail/${roomType.id}">View Details</a>
                                        <input type="hidden" value="${roomType.rooms.iterator().next().id}" name="roomId">
                                        <input type="hidden" value="${checkIn}" name="checkinStr">
                                        <input type="hidden" value="${checkOut}" name="checkoutStr">
                                        <input type="hidden" value="${!empty roomType.promotions  ? roomType.promotions.iterator().next().discount : 0}" name="discount">
                                        <input type="submit" class="btn btn-dark" style="float: right;margin-right:50px"value="BOOK">
                                    </div>
                                </div>
                            </div>
                            <button onclick="myFunction()" class="btn btn-link show_service" type="button">Service options</button>
                            <div class="row service hidden">
                                <c:if test="${!empty services}">
                                    <c:forEach var="service" items="${services}">
                                        <c:if test="${service.status eq 'ACTIVE'}">
                                        <div class="col-md-4">
                                            <label style="margin-left: 30px">
                                                <input type="checkbox"  name="serviceId" value="${service.id}" >
                                                ${service.name}
                                                ( ${service.price} VNĐ )
                                            </label>
                                        </div>
                                        </c:if>
                                    </c:forEach>
                                </c:if>
                            </div>
                        </mvc:form>
                    </c:if>
                    </c:if>
                </c:forEach>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="pagination__number">
                            <a href="#">1</a>
                            <a href="#">2</a> 
                            <a href="#">Next <span class="arrow_right"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script>
            $('.show_service').click(function ()
            {
                $('.service').toggleClass('hidden'); //Adds 'a', removes 'b' and vice versa
            });
        </script>
        <jsp:include page="include/footer.jsp" />
        <jsp:include page="include/js-page.jsp" />
    </body>
</html>
