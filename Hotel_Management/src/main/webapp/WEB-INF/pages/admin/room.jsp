<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Page</title>
        <jsp:include page="include/css-header.jsp" />
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="include/nav-top.jsp" />
            <jsp:include page="include/nav-side.jsp" />
            <div id="page-wrapper">
                <div class="header"> 
                    <h1 class="page-header">
                        Room
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Dashboard</a></li>
                        <li class="active">Data</li>
                    </ol> 
                </div>
                <div id="page-inner">
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <form action="${pageContext.request.contextPath}/admin/room/search"
                              class="form-inline">
                            <div class="form-group">
                                <input name="q" class="form-control"/>
                                <input type="submit" value="Search" 
                                       class="btn btn-info" />
                            </div>
                        </form>
                    </div>
                    <a href="${pageContext.request.contextPath}/admin/room/add">
                        <button class="btn btn-primary" style="float: right; margin-bottom: 20px;">
                            add room 
                        </button>
                    </a>
                    <div class="row" style="margin-bottom: 450px;">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <c:if test="${roomTypes.size() > 0}">
                                    <div class="row">
                                        <div class="col-lg-3"  > 
                                            <div class="form-group">
                                                <select name="name" class="form-control" onchange="location = this.value;">
                                                    <c:forEach var="roomType" items="${roomTypes}">
                                                        <option value="#${roomType}">${roomType} ROOM</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </c:if>
                                <div class="panel-body">
                                    <c:forEach items="${roomsByRoomType}" var="rooms">
                                        <c:if test="${rooms.size() > 0}">
                                            <div id="${rooms[0].roomType.name}" style="margin-bottom: 60px"></div>
                                            <h4>
                                                <b>${rooms[0].roomType.name} ROOM</b>
                                            </h4>
                                            <div class="table-responsive">
                                                <table class="table"  >
                                                    <thead>
                                                        <tr>
                                                            <th>Room Number</th>
                                                            <th>Status</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <c:forEach var="room" items="${rooms}">
                                                            <tr class="odd gradeX">
                                                                <td style="vertical-align: middle">${room.roomNumber}</td>
                                                                <td style="vertical-align: middle">
                                                                    <button onclick="location.href = '<c:url value="/admin/room/change-status/${room.id}"/>'"
                                                                            class="${room.status eq "ACTIVE" ? "btn btn-success" : "btn btn-danger"}">
                                                                        ${room.status}
                                                                    </button>
                                                                </td>
                                                                <td style="vertical-align: middle">    
                                                                    <button class="btn btn-warning"
                                                                            onclick="location.href = '<c:url value="/admin/room/update/${room.id}"/>'">
                                                                        Update
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                        </c:forEach>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </c:if>
                                    </c:forEach>
                                    <%-- search --%>
                                    <c:if test="${roomsSearch.size() > 0}">
                                        <div class="table-responsive">
                                            <table class="table" >
                                                <thead>
                                                    <tr>
                                                        <th>Room Number</th>
                                                        <th>Number of Bed</th>
                                                        <th>price</th>
                                                        <th>Status</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach var="room" items="${roomsSearch}">
                                                        <tr class="odd gradeX">
                                                            <td style="vertical-align: middle">${room.roomNumber}</td>
                                                            <td style="vertical-align: middle">${room.numberOfBed}</td>
                                                            <td style="vertical-align: middle">${room.price}</td>
                                                            <td style="vertical-align: middle">${room.status}</td>
                                                            <td style="vertical-align: middle">    
                                                                <button class="btn btn-default"
                                                                        onclick="location.href = '<c:url value="/admin/room/update/${room.id}"/>'">
                                                                    Update
                                                                </button>
                                                            </td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </c:if>
                                </div>
                            </div>
                        </div>
                    </div>
                    <jsp:include page="include/footer.jsp" />
                </div>
            </div>
            <jsp:include page="include/js-page.jsp" />
    </body>
</html>
