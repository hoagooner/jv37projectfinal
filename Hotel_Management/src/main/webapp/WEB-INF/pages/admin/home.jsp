
<%@page import="java.util.Date"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Page</title>
        <jsp:include page="include/css-header.jsp" />
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="include/nav-top.jsp" />
            <jsp:include page="include/nav-side.jsp" />
            <div id="page-wrapper">
                <div class="header"> 
                    <h1 class="page-header">
                        <small>${message}</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Dashboard</a></li>
                        <li class="active">Data</li>
                    </ol> 
                </div>
                <div id="page-inner">
                    <div class="row">
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="panel panel-primary text-center no-boder blue">
                                <i class="fa fa-home fa-5x"></i>
                                <h3>${roomQuantity}</h3>
                                <strong> Rooms</strong>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="panel panel-primary text-center no-boder blue">
                                <i class="fa fa-shopping-cart fa-5x"></i>
                                <h3>${bookingQuantity} </h3>
                                <strong>Bookings</strong>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="panel panel-primary text-center no-boder blue">
                                <i class="fa fa-users fa-5x"></i>
                                <h3>${guestQuantity} </h3>
                                <strong>Guests</strong>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2 style="color: #002752">Room Status</h2>
                                    <h4 style="color: #002752"><i class="fa fa-calendar"></i>
                                        <fmt:formatDate value="${now}" pattern="dd/MM/yyyy" />
                                    </h4>
                                </div>
                                <div class="panel-heading" style="float: right">
                                    <span style="background-color: green;color: white;padding: 10px 22px">Available</span>
                                    <span style="background-color: red;color: white;padding: 10px 22px">Booked</span>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">

                                        <div>
                                            <table class="table"  >
                                                <thead>
                                                    <tr>
                                                        <th style="width: 20%">Room Type</th>
                                                        <th style="width: 80%">Room</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach items="${roomsByRoomType}" var="rooms">
                                                        <c:if test="${rooms.size() > 0}">
                                                            <tr>
                                                                <td style="width: 20%">${rooms[0].roomType.name}</td>
                                                                <td style="width: 80%">
                                                                    <c:forEach var="room" items="${rooms}">
                                                                        <%
                                                                            String style = "btn btn-success";
                                                                        %>
                                                                        <c:forEach items="${bookedRooms}" var="bookedRoom">
                                                                            <c:if test="${bookedRoom.id == room.id}">
                                                                                <%
                                                                                    style = "btn btn-danger";
                                                                                %>
                                                                            </c:if>
                                                                        </c:forEach>
                                                                        <button class="<%=style%>" style="padding: 10px 26px;cursor: default">
                                                                            ${room.roomNumber}
                                                                        </button>
                                                                    </c:forEach> 
                                                                </td>
                                                            </tr>
                                                        </c:if>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div> 
                </div>
            </div>
        </div>
        <jsp:include page="include/js-page.jsp" />
    </body>
</html>
