
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="mvc"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Page</title>
        <style>
        </style>
        <jsp:include page="include/css-header.jsp" />
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="include/nav-top.jsp" />
            <jsp:include page="include/nav-side.jsp" />
            <div id="page-wrapper">
                <div class="header"> 
                    <h1 class="page-header">
                        Room 
                    </h1>
                </div>
                <div id="page-inner"> 
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <c:if test="${action == 'add'}">
                                                <h3>Create Room </h3>
                                            </c:if>
                                            <c:if test="${action == 'update'}">
                                                <h3>Update Room </h3>
                                            </c:if>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <mvc:form action="${pageContext.request.contextPath}/admin/room/result"
                                              method="post" modelAttribute="room">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Room Type</label>
                                                    <select name="roomType.id" class="form-control">
                                                        <c:if test="${!empty room.roomType }">
                                                            <option value="${room.roomType.id}" selected>${room.roomType.name}</option> 
                                                        </c:if>
                                                        <c:forEach var="roomType" items="${roomTypes}">
                                                            <c:if test="${roomType.id != room.roomType.id}">
                                                                <option value="${roomType.id}">${roomType.name}</option>
                                                            </c:if>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Enter Room Number</label>
                                                    <input class="form-control" name="roomNumber" value="${room.roomNumber}" placeholder="Enter name">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Status</label>
                                                    <select name="status" class="form-control">
                                                        <option selected="${room.status}">${room.status}</option> 
                                                        <c:forEach var="status" items="${statuses}">
                                                            <c:if test="${status != room.status}">
                                                                <option value="${status}">${status}</option>
                                                            </c:if>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <c:if test="${action == 'add'}">
                                            <input type="hidden" value="add" name="action">
                                            <button type="submit" class="btn btn-primary">
                                                Create Room 
                                            </button>
                                        </c:if>
                                        <c:if test="${action == 'update'}">
                                            <input type="hidden" value="update" name="action">
                                            <input type="hidden" value="${room.id}" name="id">
                                            <button class="btn btn-primary" type="submit">
                                                Update Room 
                                            </button>
                                        </c:if>
                                        <button type="reset" class="btn btn-warning"
                                                onclick="location.href = '<c:url value="/admin/room"/>'">
                                            Cancel
                                        </button>
                                    </mvc:form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <jsp:include page="include/footer.jsp" />
        </div>
    </div>
    <jsp:include page="include/js-page.jsp" />
</body>
</html>
