
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="mvc"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reservation Detail</title>
        <jsp:include page="include/css-header.jsp" />
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="include/nav-top.jsp" />
            <jsp:include page="include/nav-side.jsp" />
            <div id="page-wrapper">

                <div class="row">
                    <div class="col-md-6" style="padding-left: 40px">
                        <h4><b>Reservation Number:</b> ${booking.bookingNumber}</h4>
                        <table class="table" style="width: 60%;margin-left: 40px">
                            <tr>
                                <td><b>Number of Rooms:</b></td>
                                <td>${booking.bookingDetails.size()}</td>
                            </tr>
                            <tr>
                                <td><b>Booking Date:</b></td>
                                <td>${booking.bookingDate}</td>
                            </tr>
                            <tr>
                                <td><b>Check In:</b> </td>
                                <td>${booking.checkIn}</td>
                            </tr>
                            <tr>
                                <td><b>Check Out:</b> </td>
                                <td>${booking.checkOut}</td>
                            </tr>
                            <tr>
                                <td><b>Total Price:</b> </td>
                                <td>${booking.totalPrice} VND</td>
                            </tr>
                            <tr>
                                <td><b>Paid Payment: </b></td>
                                <td>${paidPayment} VND</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <h4><b>Guest Information</b></h4>
                        <h5>${booking.fullName}</h5>
                        <h5><b>email:</b> ${booking.email}</h5>
                        <h5><b>phone:</b> ${booking.phoneNumber}</h5>
                        <h5><b>address</b> ${booking.address}</h5>
                    </div>
                </div>
                
                <div id="page-inner"> 
                    <div class="card-body">
                        <!-- PRODUCT -->
                        <c:forEach var="bookingDetail" items="${booking.bookingDetails}">
                            <mvc:form action="${pageContext.request.contextPath}/admin/reservation/update-service/${bookingDetail.id}"
                                      method="post" modelAttribute="booking" >
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-2 text-center">
                                        <c:if test="${! empty bookingDetail.room.roomType.images}">
                                            <img class="img-responsive" 
                                                 src="<c:url value='/upload/images/${bookingDetail.room.roomType.images.iterator().next().name}' />"
                                                 alt="prewiew" width="230" height="150">
                                        </c:if>
                                    </div>
                                    <div class="col-12 text-sm-center col-sm-12 text-md-left col-md-6">
                                        <h4 class="product-name"><strong>${bookingDetail.room.roomType.name} ROOM</strong></h4>
                                        <h4>
                                            <small>Room Number: ${bookingDetail.room.roomNumber}</small>
                                        </h4>
                                        <h4>
                                            <small>Room Price ${bookingDetail.room.roomType.price}</small>
                                        </h4>
                                    </div>
                                    <div class="col-12 col-sm-12 text-sm-center col-md-4 text-md-right row">
                                        <div class="col-3 col-sm-3 col-md-6 text-md-right" style="padding-top: 5px">
                                            <h5><strong>${bookingDetail.price} VNĐ</strong></h5>
                                        </div>
                                    </div>
                                </div>
                                <h4>Extra Service</h4>
                                <div class="row" style="margin-bottom: 30px">
                                    <c:forEach var="service" items="${services}">
                                        <%
                                            String str = "";
                                        %>
                                        <c:forEach var="s" items="${bookingDetail.services}">
                                            <c:if test="${s.name == service.name}">
                                                <%
                                                    str = "checked";
                                                %>
                                            </c:if>
                                        </c:forEach>
                                        <div class="col-md-4">
                                            <label style="margin-left: 40px">
                                                <input type="checkbox"  <%=str%> name="serviceId" value="${service.id}" >
                                                ${service.name}
                                                (${service.price} VNĐ )
                                            </label>
                                        </div>
                                    </c:forEach>
                                </div> 
                                <div class="row">
                                    <div class="col-md-12" >
                                        <input type="hidden" name="bookingId" value="${booking.id}">
                                        <input type="submit" class="btn btn-success"style="float: right;margin-right: 50px" value="update sevice">
                                    </div>
                                </div>
                                <hr>
                            </mvc:form>
                        </c:forEach>
                        <hr>
                    </div>
                    <jsp:include page="include/footer.jsp" />
                </div>
            </div>
        </div>
        <jsp:include page="include/js-page.jsp" />
    </body>
</html>
