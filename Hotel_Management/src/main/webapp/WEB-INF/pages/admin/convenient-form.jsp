
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="mvc"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Convenient Form</title>
        <jsp:include page="include/css-header.jsp" />
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="include/nav-top.jsp" />
            <jsp:include page="include/nav-side.jsp" />
            <div id="page-wrapper">
                <div class="header"> 
                    <h1 class="page-header">
                        Convenient
                    </h1>
                </div>
                <div id="page-inner"> 
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <c:if test="${action == 'add'}">
                                                <h3>Create Convenient</h3>
                                            </c:if>
                                            <c:if test="${action == 'update'}">
                                                <h3>Update Convenient</h3>
                                            </c:if>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <mvc:form action="${pageContext.request.contextPath}/admin/convenient/result"
                                              method="post" modelAttribute="convenient" enctype="multipart/form-data">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Convenient</label>
                                                    <input class="form-control" name="name" value="${convenient.name}" placeholder="Enter text">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Status</label>
                                                    <select name="status" class="form-control">
                                                        <c:if test="${!empty promotion.status }">
                                                            <option selected="${promotion.status}">${promotion.status}</option> 
                                                        </c:if>
                                                        <c:forEach var="status" items="${statuses}">
                                                            <c:if test="${status != promotion.status}">
                                                                <option value="${status}">${status}</option>
                                                            </c:if>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>Description</label>
                                                    <textarea class="form-control" name="description" rows="3">${convenient.description}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>Image</label>
                                                    <input type="file" name="file">
                                                </div>
                                            </div>
                                        </div>
                                        <c:if test="${not empty roomType.images}">
                                            <c:forEach var="image" items="${roomType.images}">
                                                <img style="width: 60px" class="img" 
                                                     src="<c:url value='/upload/images/${image.name}' />" alt="image" />
                                                <button type="submit" 
                                                        formaction="${pageContext.request.contextPath}/admin/room-type/image/delete/${image.id}"
                                                        style="border: none; background-color: white; outline:none;">
                                                    <i class="fa fa-trash-o" style="color: red"></i> 
                                                </button>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${action == 'add'}">
                                            <input type="hidden" value="add" name="action">
                                            <button type="submit" class="btn btn-default">
                                                Create Convenient
                                            </button>
                                        </c:if>
                                        <c:if test="${action == 'update'}">
                                            <input type="hidden" value="update" name="action">
                                            <input type="hidden" value="${convenient.id}" name="id">
                                            <button class="btn btn-default" type="submit">
                                                Update Convenient     
                                            </button>
                                        </c:if>
                                        <button type="reset" class="btn btn-default">Reset Button</button>
                                    </mvc:form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <jsp:include page="include/footer.jsp" />
                </div>
            </div>
        </div>
        <jsp:include page="include/js-page.jsp" />
    </body>
</html>
