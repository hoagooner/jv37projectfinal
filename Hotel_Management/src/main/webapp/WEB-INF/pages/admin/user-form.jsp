
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="mvc"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Page</title>
        <style>
        </style>
        <jsp:include page="include/css-header.jsp" />
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="include/nav-top.jsp" />
            <jsp:include page="include/nav-side.jsp" />
            <div id="page-wrapper">
                <div class="header"> 
                    <h1 class="page-header">
                        Add User
                    </h1>
                </div>
                <div id="page-inner"> 
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                
                                <div class="panel-body">
                                    <mvc:form action="${pageContext.request.contextPath}/admin/add-user"
                      method="post" modelAttribute="user">
                <div class="single-product-tab-area mg-b-30">
                    <!-- Single pro tab review Start-->
                    <div class="single-pro-review-area">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="review-tab-pro-inner">
                                        <ul id="myTab3" class="tab-review-design">
                                            <li class="active"><a href="#description"><i class="icon nalika-edit" aria-hidden="true"></i>User</a></li>
                                        </ul>
                                        <div id="myTabContent" class="tab-content custom-product-edit">
                                            <div class="product-tab-list tab-pane fade active in" id="description">
                                                <div class="row">
                                                    <div class="review-content-section " style="width: 700px;">
                                                        <div class="text-center">
                                                            <div class="input-group mg-b-pro-edt">
                                                                <span class="input-group-addon"><i class="icon nalika-user" aria-hidden="true"></i></span>
                                                                <input name="fullName" type="text" class="form-control" placeholder="Name" required="">
                                                            </div>
                                                            <div class="input-group mg-b-pro-edt">
                                                                <span class="input-group-addon"><i class="icon nalika-favorites-button" aria-hidden="true"></i></span>
                                                                <input name="email" type="text" class="form-control" placeholder="E-mail" required="">
                                                            </div>
                                                            <div class="input-group mg-b-pro-edt" style="background-color: #152036; color: white">
                                                                <c:forEach var="role" items="${roles}">
                                                                    <% String check = ""; %>
                                                                    <c:forEach items="${user.getUserRoles()}" var="roleUser"> 
                                                                        <c:if test="${roleUser.id == role.id}">
                                                                            <% check = "checked";%>
                                                                        </c:if>
                                                                    </c:forEach>
                                                                    <div class="col-lg-6">
                                                                        <c:if test="${role.getRole() == 'ROLE_USER'}">
                                                                            <div class="checkbox">
                                                                                <label>
                                                                                    <input style="background-color: blue" type="checkbox" checked="true" name="" value="" disabled="">
                                                                                    <input type="checkbox" checked name="role" value="${role.id}" hidden="true">
                                                                                    ${role.getRole()}
                                                                                </label>
                                                                            </div>
                                                                        </c:if>
                                                                        <c:if test="${role.getRole() != 'ROLE_USER'}">
                                                                            <div class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" <%=check%> name="role" value="${role.id}">
                                                                                    ${role.getRole()}
                                                                                </label>
                                                                            </div>
                                                                        </c:if>
                                                                    </div>
                                                                </c:forEach>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row"><br/></div>
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="text-center custom-pro-edt-ds">
                                                            <input type="hidden" value="add" name="action">
                                                            <button class="btn btn-ctl-bt waves-effect waves-light m-r-10" type="submit">
                                                                Create User    
                                                            </button>
                                                            <div class="btn btn-ctl-bt waves-effect waves-light m-r-10">
                                                                <a style="color: white" href="<c:url value="/admin/user_manage"/>">Discard</a>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </mvc:form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <jsp:include page="include/footer.jsp" />
        </div>
    </div>
    <jsp:include page="include/js-page.jsp" />
</body>
</html>
