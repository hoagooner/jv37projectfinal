<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Page</title>
        <jsp:include page="include/css-header.jsp" />
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="include/nav-top.jsp" />
            <jsp:include page="include/nav-side.jsp" />
            <div id="page-wrapper">
                <div class="header"> 
                    <h1 class="page-header">
                        Room Type
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Dashboard</a></li>
                        <li class="active">Data</li>
                    </ol> 
                </div>
                <div id="page-inner">
                    <a href="${pageContext.request.contextPath}/admin/room-type/add">
                        <button class="btn btn-primary" style="float: right; margin-bottom: 20px;">
                            add room type
                        </button>
                    </a>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Room Type List
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table" >
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Image</th>
                                                    <th>size</th>
                                                    <th>price</th>
                                                    <th>Number Of Bed</th>
                                                    <th>Occupancy</th>
                                                    <th>Status</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach var="roomType" items="${roomTypes}">
                                                    <tr class="odd gradeX">
                                                        <td style="vertical-align: middle">${roomType.name}</td>
                                                        <td style="vertical-align: middle">
                                                            <c:if test="${!empty roomType.images}">
                                                                <img style="width: 70px;height: 70px;display: inline-block;" 
                                                                     src="<c:url value='/upload/images/${roomType.images.iterator().next().name}' />"
                                                                     alt="image" />
                                                            </c:if>    
                                                        </td>
                                                        <td style="vertical-align: middle">${roomType.size}</td>
                                                        <td style="vertical-align: middle">${roomType.price}</td>
                                                        <td style="vertical-align: middle">${roomType.numberOfBed}</td>
                                                        <td style="vertical-align: middle">${roomType.occupancy}</td>
                                                        <td style="vertical-align: middle">
                                                            <button onclick="location.href = '<c:url value="/admin/room-type/change-status/${roomType.id}"/>'"
                                                                    class="${roomType.status eq "ACTIVE" ? "btn btn-success" : "btn btn-danger"}">
                                                                ${roomType.status}
                                                            </button>
                                                            </a>
                                                        </td>
                                                        <td style="vertical-align: middle">    
                                                            <button class="btn btn-warning"
                                                                    onclick="location.href = '<c:url value="/admin/room-type/update/${roomType.id}"/>'">
                                                                Update
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!--End Advanced Tables -->
                        </div>
                    </div>
                    <jsp:include page="include/footer.jsp" />
                </div>
            </div>
            <jsp:include page="include/js-page.jsp" />
    </body>
</html>
