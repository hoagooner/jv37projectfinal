<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
            <li>
                <a class="active-menu" href="${pageContext.request.contextPath}/admin/home"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/admin/reservation"><i class="fa fa-bar-chart-o"></i> Reservation</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-sitemap"></i> Hotel Configuration<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="${pageContext.request.contextPath}/admin/room-type">Room Type</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/admin/room">Room</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/admin/service">Service</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/admin/convenient">Convenient</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/admin/promotion">Promotion</a>
                    </li>
                </ul>
            </li>

            <li class="active">
                <a class="has-arrow" href="${pageContext.request.contextPath}/admin/user_manage" aria-expanded="false"><i class="icon nalika-user"></i> 
                    <span class="mini-click-non">Account Management</span></a>
            </li>

        </ul>
    </div>
</nav>