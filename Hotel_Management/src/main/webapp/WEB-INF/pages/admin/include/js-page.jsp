<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script  src="<c:url value="/resources-management/assets/js/jquery-1.10.2.js"/>"></script>
<!-- Bootstrap Js -->
<script src="<c:url value="/resources-management/assets/js/bootstrap.min.js"/>"></script>
<!-- Metis Menu Js -->
<script src="<c:url value="/resources-management/assets/js/jquery.metisMenu.js"/>"></script>
<!-- Morris Chart Js -->
<script src="<c:url value="/resources-management/assets/js/morris/raphael-2.1.0.min.js"/>"></script>
<script src="<c:url value="/resources-management/assets/js/morris/morris.js"/>"></script>
<script src="<c:url value="/resources-management/assets/js/easypiechart.js"/>"></script>
<script src="<c:url value="/resources-management/assets/js/easypiechart-data.js"/>"></script>
<script src="<c:url value="/resources-management/assets/js/Lightweight-Chart/jquery.chart.js"/>"></script>
<!-- Custom Js -->
<script src="<c:url value="/resources-management/assets/js/custom-scripts.js"/>"></script>