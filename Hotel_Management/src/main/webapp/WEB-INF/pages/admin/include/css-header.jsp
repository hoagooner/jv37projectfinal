<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- Bootstrap Styles-->
<link href="<c:url value="/resources-management/assets/css/bootstrap.css"/>" rel="stylesheet" />
<!-- FontAwesome Styles-->
<link href="<c:url value="/resources-management/assets/css/font-awesome.css"/>" rel="stylesheet" />
<!-- Morris Chart Styles-->
<link href="<c:url value="/resources-management/assets/js/morris/morris-0.4.3.min.css"/>" rel="stylesheet" />
<!-- Custom Styles-->
<link href="<c:url value="/resources-management/assets/css/custom-styles.css"/>" rel="stylesheet" />
<!-- Google Fonts-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="<c:url value="/resources-management/assets/js/Lightweight-Chart/cssCharts.css"/>">
 <link rel="stylesheet" href="<c:url value="/resources/fonts/ionicons/css/ionicons.min.css"/>">