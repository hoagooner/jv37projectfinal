
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="mvc"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Page</title>
        <jsp:include page="include/css-header.jsp" />
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="include/nav-top.jsp" />
            <jsp:include page="include/nav-side.jsp" />
            <div id="page-wrapper">
                <div class="header">
                    <h1 class="page-header">Promotion</h1>
                </div>
                <div id="page-inner">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <c:if test="${action == 'add'}">
                                                <h3>Create Promotion</h3>
                                            </c:if>
                                            <c:if test="${action == 'update'}">
                                                <h3>Update Promotion</h3>
                                            </c:if>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <mvc:form
                                        action="${pageContext.request.contextPath}/admin/promotion/result"
                                        method="post" modelAttribute="promotion"
                                        enctype="multipart/form-data">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Promotion</label> <input class="form-control" required
                                                                                    value="${promotion.name}" name="name"
                                                                                    placeholder="Enter name">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Discount (%)</label> <input class="form-control" required type="number"
                                                                                       value="${promotion.discount}" name="discount"
                                                                                       placeholder="Enter discount (%)">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="startDate">Start Date:</label>
                                                    <fmt:formatDate value="${promotion.startDate}"
                                                                    pattern="yyyy-MM-dd" var="startDate" />
                                                    <input type="date" class="form-control" pattern="yyyy-MM-dd" required
                                                           id="startDate" name="startDate" value="${startDate}"
                                                           ${action == "update" ?  "readonly" : ""} />
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="endDate">End Date:</label>
                                                    <fmt:formatDate value="${promotion.endDate}"
                                                                    pattern="yyyy-MM-dd" var="endDate" />
                                                    <input type="date" class="form-control" pattern="yyyy-MM-dd" required
                                                           id="endDate" name="endDate" value="${endDate}"
                                                           ${action == "update" ?  "readonly" : ""} />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea class="form-control" name="description" rows="3">${promotion.description}</textarea>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Image</label> <input type="file" name="file">
                                                </div>
                                                <c:if test="${!empty promotion.images}">
                                                    <c:forEach var="image" items="${promotion.images}">
                                                        <img style="width: 60px"
                                                             src="<c:url value='/upload/images/${image.name}' />"
                                                             alt="image" />
                                                        <button type="submit"
                                                                formaction="${pageContext.request.contextPath}/admin/promotion/image/delete/${image.id}"
                                                                style="border: none; background-color: white; outline: none;">
                                                            <i class="fa fa-trash-o" style="color: red"></i>
                                                        </button>
                                                    </c:forEach>
                                                </c:if>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>Choose room types for your this promotion: </label><br>
                                                    <c:if test="${! empty invalidRoomTypes}">
                                                        <div class="alert alert-danger">
                                                            <c:forEach var="invalidRoomType"
                                                                       items="${invalidRoomTypes}">
                                                                ${invalidRoomType.name} ROOM
                                                            </c:forEach>
                                                            has promotion during this promotion time
                                                        </div>
                                                    </c:if>
                                                    <div class="col-lg-6">
                                                        <table class="table">
                                                            <c:forEach var="roomType" items="${roomTypes}">
                                                                <%
                                                                    String check = "";
                                                                    String c1 = "";
                                                                %>
                                                                <c:forEach var="room_promo" items="${promotion.roomTypes}">
                                                                    <c:if test="${roomType.id == room_promo.id}">
                                                                        <%
                                                                            check = "checked";
                                                                        %>
                                                                    </c:if>
                                                                </c:forEach>
                                                                <c:forEach var="r" items="${invalidRoomTypes}">
                                                                    <c:if test="${r.name eq roomType.name}">
                                                                        <%
                                                                            c1 = "alert alert-danger";
                                                                        %>
                                                                    </c:if>
                                                                </c:forEach>
                                                                <tr class="<%= c1%>">
                                                                    <td style="vertical-align: middle;"><input <%=check%>
                                                                            type="checkbox" id="${roomType.id}" name="roomType"
                                                                            value="${roomType.id}"></td>
                                                                        <c:if test="${! empty roomType.images}">
                                                                        <td style="vertical-align: middle">
                                                                            <img style="width: 90px; height: 90px;"
                                                                                 src="<c:url value='/upload/images/${roomType.images.iterator().next().name}' />"
                                                                                 alt="image" />
                                                                        </td>
                                                                    </c:if>
                                                                    <td style="vertical-align: middle"><label
                                                                            for="${roomType.id}">${roomType.name}</label>
                                                                    </td>
                                                                </tr>                                                          
                                                            </c:forEach>
                                                        </table>
                                                    </div>
                                                    <%--</c:if>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <c:if test="${action == 'add'}">
                                            <input type="hidden" value="add" name="action">
                                            <button type="submit" class="btn btn-primary">Create
                                                Promotion</button>
                                            </c:if>
                                            <c:if test="${action == 'update'}">
                                            <input type="hidden" value="update" name="action">
                                            <input type="hidden" value="${promotion.id}" name="id">
                                            <button class="btn btn-primary" type="submit">Update
                                                Promotion</button>
                                            </c:if>
                                        <button type="reset" class="btn btn-warning"
                                                onclick="location.href = '<c:url value="/admin/promotion"/>'">
                                            Cancel</button>
                                        </mvc:form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <jsp:include page="include/footer.jsp" />
            </div>
        </div>
        <jsp:include page="include/js-page.jsp" />
    </body>
</html>
