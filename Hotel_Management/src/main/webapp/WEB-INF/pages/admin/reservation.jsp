<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="mvc"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reservation</title>
        <style>
        </style>
        <jsp:include page="include/css-header.jsp" />
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="include/nav-top.jsp" />
            <jsp:include page="include/nav-side.jsp" />
            <div id="page-wrapper">
                <div class="header"> 
                    <h1 class="page-header">
                        Reservation
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Dashboard</a></li>
                        <li class="active">Reservation</li>
                    </ol> 
                </div>
                <div id="page-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Reservation
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="${pageContext.request.contextPath}/admin/reservation/search-booking-date" method="POST" 
                                              class="form-inline" style="float: right;margin-right: 10px">
                                            <div class="form-group">
                                                <input type="date" id="fromDate" name="fromDate" class="form-control" value="${fromDate}" required="true"/>
                                                <input type="date" id="toDate" name="toDate" class="form-control" value="${toDate}" required="true"/>
                                                <input type="submit" value="Search" class="btn btn-info" />
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <c:if test="${fromDate != null && toDate != null && bookings.size() > 0}">
                                            <mvc:form
                                                action="${pageContext.request.contextPath}/admin/reservation/export-file"
                                                method="post">
                                                <button style="float: right;margin: 10px" class="btn btn-primary"
                                                        type="submit">
                                                    export
                                                </button>
                                            </mvc:form>
                                        </c:if>
                                    </div>
                                </div>

                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table" >
                                            <thead>
                                                <tr>
                                                    <th>Reservation No.</th>
                                                    <th>Guest</th>
                                                    <th>Reservation Date</th>
                                                    <th>Check In</th>
                                                    <th>Check Out</th>                                                    
                                                    <th>No. of Rooms</th>
                                                    <th>Booking Type</th>
                                                    <th>Reservation Status</th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach var="booking" items="${bookings}">
                                                    <tr class="odd gradeX">
                                                        <td style="vertical-align: middle">${booking.bookingNumber}</td>
                                                        <td style="vertical-align: middle">${booking.fullName}</td>
                                                        <td style="vertical-align: middle">${booking.bookingDate}</td>
                                                        <td style="vertical-align: middle">${booking.checkIn}</td>
                                                        <td style="vertical-align: middle">${booking.checkOut}</td>
                                                        <td style="vertical-align: middle">${booking.bookingDetails.size()}</td>
                                                        <td style="vertical-align: middle">${booking.bookingType}</td>
                                                <form action="${pageContext.request.contextPath}/admin/reservation/change-status/${booking.id}" method="post">
                                                    <td style="vertical-align: middle">
                                                        <select name="status" class="form-control" ${booking.status eq 'CANCEL' or  booking.status eq 'CHECK_OUT' ? 'disabled' : ''}>
                                                            <option selected value="${booking.status}">${booking.status}</option>
                                                            <c:forEach var="status" items="${bookingStatus}">
                                                                <c:if test="${status != booking.status }">
                                                                    <option value="${status}">${status}</option>
                                                                </c:if>
                                                            </c:forEach>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <button type="submit" style="border: none;background-color: transparent;outline:none;">
                                                            <span class="ion-android-refresh" style="font-size:  25px"></span>
                                                        </button>
                                                    </td>
                                                </form>
                                                <td style="vertical-align: middle">    
                                                    <button class="btn btn-warning"
                                                            onclick="location.href = '<c:url value="/admin/reservation/${booking.id}"/>'">
                                                        view
                                                    </button>
                                                    </a>
                                                </td>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!--End Advanced Tables -->
                        </div>
                    </div>
                    <jsp:include page="include/footer.jsp" />
                </div>
            </div>
            <jsp:include page="include/js-page.jsp" />
    </body>
</html>
