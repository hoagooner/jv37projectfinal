
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="mvc"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Page</title>
        <style>
        </style>
        <jsp:include page="include/css-header.jsp" />
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="include/nav-top.jsp" />
            <jsp:include page="include/nav-side.jsp" />
            <div id="page-wrapper">
                <div class="header"> 
                    <h1 class="page-header">
                        Room Type
                    </h1>
                </div>
                <div id="page-inner"> 
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <c:if test="${error != null}">
                                    <div class="alert alert-danger">${error}</div>
                                </c:if>
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <c:if test="${action == 'add'}">
                                                <h3>Create Room Type</h3>
                                            </c:if>
                                            <c:if test="${action == 'update'}">
                                                <h3>Update Room Type</h3>
                                            </c:if>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <mvc:form action="${pageContext.request.contextPath}/admin/room-type/result"
                                              method="post" modelAttribute="roomType" enctype="multipart/form-data">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Room Type</label>
                                                    <select name="name" class="form-control">
                                                        <option value="${roomType.name}" selected>${roomType.name} ROOM</option> 
                                                        <c:forEach var="roomTypeEnum" items="${roomTypesEnum}">
                                                            <c:if test="${roomTypeEnum != roomType.name}">
                                                                <option value="${roomTypeEnum}">${roomTypeEnum} ROOM</option>
                                                            </c:if>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Status</label>
                                                    <select name="status" class="form-control">
                                                        <option value="${roomType.status}" selected>${roomType.status}</option> 
                                                        <c:forEach var="status" items="${statuses}">
                                                            <c:if test="${status != roomType.status}">
                                                                <option value="${status}">${status}</option>
                                                            </c:if>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Maximum Occupancy</label>
                                                    <input class="form-control" name="occupancy" value="${roomType.occupancy}" placeholder="Enter Occupancy">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Number Of Bed</label>
                                                    <input class="form-control" name="numberOfBed" value="${roomType.numberOfBed}" placeholder="Enter Size">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Size</label>
                                                    <input class="form-control" name="size" value="${roomType.size}" placeholder="Enter Size">
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Price</label>
                                                    <input class="form-control" name="price" value="${roomType.price}" placeholder="Enter Size">
                                                </div>
                                            </div>

                                        </div>


                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea class="form-control" name="description" rows="3">${roomType.description}</textarea>
                                        </div>

                                        <div class="row" style="margin-bottom: 40px">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Convenient</label><br>
                                                    <c:if test="${action == 'add'}">
                                                        <c:forEach var="convenient" items="${convenients}">
                                                            <c:if test="${convenient.status eq 'ACTIVE'}">
                                                            <div class="col-lg-6">
                                                                <input type="checkbox" id="${convenient.id}"  name="convenient" value="${convenient.id}">
                                                                <label for="${convenient.id}">${convenient.name}</label><br>
                                                            </div>    
                                                            </c:if>
                                                        </c:forEach>
                                                    </c:if>
                                                    <c:if test="${action == 'update'}">
                                                        <c:forEach var="convenient" items="${convenients}">
                                                            <c:if test="${convenient.status eq 'ACTIVE'}">
                                                            <%
                                                                String check = "";
                                                            %>
                                                            <c:forEach var="convenientOfRoomType" items="${convenientsOfRoomType}">
                                                                
                                                                <c:if test="${convenient.id == convenientOfRoomType.id}">
                                                                    <%
                                                                        check = "checked";
                                                                    %>
                                                                </c:if>
                                                            </c:forEach>
                                                            <div class="col-lg-6">
                                                                <div class="checkbox">
                                                                    <label><input type="checkbox" <%=check%>   name="convenient" value="${convenient.id}">
                                                                        ${convenient.name}
                                                                    </label>
                                                                </div>
                                                            </div> 
                                                            </c:if>
                                                        </c:forEach>

                                                    </c:if>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Image</label>
                                                    <input type="file" name="file">
                                                </div>
                                            </div>
                                            <c:if test="${not empty roomType.images}">
                                                <c:forEach var="image" items="${roomType.images}">
                                                    <img style="width: 60px" class="img" 
                                                         src="<c:url value='/upload/images/${image.name}' />" alt="image" />
                                                    <button type="submit" 
                                                            formaction="${pageContext.request.contextPath}/admin/room-type/image/delete/${image.id}"
                                                            style="border: none; background-color: white; outline:none;">
                                                        <i class="fa fa-trash-o" style="color: red"></i> 
                                                    </button>
                                                </c:forEach>
                                            </c:if>
                                        </div>
                                        <c:if test="${action == 'add'}">
                                            <input type="hidden" value="add" name="action">
                                            <button type="submit" class="btn btn-primary">
                                                Create Room Type
                                            </button>
                                        </c:if>
                                        <c:if test="${action == 'update'}">
                                            <input type="hidden" value="update" name="action">
                                            <input type="hidden" value="${roomType.id}" name="id">
                                            <button class="btn btn-primary" type="submit">
                                                Update Room Type
                                            </button>
                                        </c:if>
                                        <button type="reset" class="btn btn-warning"
                                                onclick="location.href = '<c:url value="/admin/room-type"/>'">
                                            Cancel
                                        </button>
                                    </mvc:form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <jsp:include page="include/footer.jsp" />
        </div>
    </div>
    <jsp:include page="include/js-page.jsp" />
</body>
</html>
