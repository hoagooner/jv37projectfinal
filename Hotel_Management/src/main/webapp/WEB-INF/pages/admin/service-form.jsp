<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="mvc"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Page</title>
        <jsp:include page="include/css-header.jsp" />
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="include/nav-top.jsp" />
            <jsp:include page="include/nav-side.jsp" />
            <div id="page-wrapper">
                <div class="header"> 
                    <h1 class="page-header">
                        Service
                    </h1>
                </div>
                <div id="page-inner"> 
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <c:if test="${action == 'add'}">
                                                <h3>Create Service</h3>
                                            </c:if>
                                            <c:if test="${action == 'update'}">
                                                <h3>Update Service</h3>
                                            </c:if>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <mvc:form action="${pageContext.request.contextPath}/admin/service/result"
                                              method="post" modelAttribute="service" enctype="multipart/form-data">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Service</label>
                                                    <input class="form-control" value="${service.name}" name="name" placeholder="Enter name">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Position</label>
                                                    <input class="form-control" value="${service.position}" name="position" placeholder="Enter Postion">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Price</label>
                                                    <input class="form-control" name="price" value="${service.price}" placeholder="Enter price">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Image</label>
                                                    <input type="file" name="file">
                                                </div>
                                            </div>
                                            <c:if test="${not empty service.images}">
                                                <c:forEach var="image" items="${service.images}">
                                                    <img style="width: 60px" class="img" 
                                                         src="<c:url value='/upload/images/${image.name}' />" alt="image" />
                                                    <button type="submit" 
                                                            formaction="${pageContext.request.contextPath}/admin/service/image/delete/${image.id}"
                                                            style="border: none; background-color: white; outline:none;">
                                                        <i class="fa fa-trash-o" style="color: red"></i> 
                                                    </button>
                                                </c:forEach>
                                            </c:if>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="openTime">Open Time:</label>
                                                    <fmt:formatDate value="${service.openTime}" pattern="HH:mm"  var="openTime" />
                                                    <input type="time" class="form-control" pattern="HH:mm"  
                                                           id="openTime" name="openTime" value="${openTime}" />
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="closeTime">Close Time:</label>
                                                    <fmt:formatDate value="${service.closeTime}" pattern="HH:mm" var="closeTime" />
                                                    <input type="time" class="form-control" pattern="HH:mm"
                                                           id="closeTime" name="closeTime" value="${closeTime}" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea class="form-control"  name="description" rows="3">${service.description}</textarea>
                                        </div>
                                        <c:if test="${action == 'add'}">
                                            <input type="hidden" value="add" name="action">
                                            <button type="submit" class="btn btn-default">
                                                Create Service
                                            </button>
                                        </c:if>
                                        <c:if test="${action == 'update'}">
                                            <input type="hidden" value="update" name="action">
                                            <input type="hidden" value="${service.id}" name="id">
                                            <button class="btn btn-default" type="submit">
                                                Update Service     
                                            </button>
                                        </c:if>
                                        <button type="reset" class="btn btn-default">Reset Button</button>
                                    </mvc:form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <jsp:include page="include/footer.jsp" />
                </div>
            </div>
        </div>
        <jsp:include page="include/js-page.jsp" />
    </body>
</html>
