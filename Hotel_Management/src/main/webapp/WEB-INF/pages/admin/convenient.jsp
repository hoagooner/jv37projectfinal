
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Page</title>
        <jsp:include page="include/css-header.jsp" />
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="include/nav-top.jsp" />
            <jsp:include page="include/nav-side.jsp" />
            <div id="page-wrapper">
                <div class="header"> 
                    <h1 class="page-header">
                        Convenient
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Dashboard</a></li>
                        <li class="active">Data</li>
                    </ol> 
                </div>
                <div id="page-inner">
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <form action="${pageContext.request.contextPath}/admin/convenient/search" class="form-inline">
                            <div class="form-group">
                                <input name="q" class="form-control"/>
                                <input type="submit" value="Search" 
                                       class="btn btn-info" />
                            </div>
                        </form>
                    </div>
                    <a href="${pageContext.request.contextPath}/admin/convenient/add">
                        <button class="btn btn-default" style="float: right; margin-bottom: 20px;">
                            add convenient
                        </button>
                    </a>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Convenient List
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Description</th>
                                                    <th>Images</th>
                                                    <th>Status</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach var="convenient" items="${convenients}">
                                                    <tr class="odd gradeX">
                                                        <td style="vertical-align: middle">${convenient.name}</td>
                                                        <td style="vertical-align: middle">${convenient.description}</td>
                                                         <td style="vertical-align: middle">
                                                            <c:if test="${!empty convenient.images}">
                                                                <img style="width: 70px;height: 70px;display: inline-block;" 
                                                                     src="<c:url value='/upload/images/${convenient.images.iterator().next().name}' />"
                                                                     alt="image" />
                                                            </c:if>    
                                                        </td>
                                                        <td style="vertical-align: middle">
                                                            <button onclick="location.href = '<c:url value="/admin/convenient/change-status/${convenient.id}"/>'"
                                                                    class="${convenient.status eq "ACTIVE" ? "btn btn-success" : "btn btn-danger"}">
                                                                ${convenient.status}
                                                            </button>
                                                        </td>
                                                        <td style="vertical-align: middle">    
                                                            <button class="btn btn-default"
                                                                    onclick="location.href = '<c:url value="/admin/convenient/update/${convenient.id}"/>'">
                                                                Update
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <jsp:include page="include/footer.jsp" />
                            </div>
                        </div>
                        <jsp:include page="include/js-page.jsp" />
                        </body>
                        </html>
