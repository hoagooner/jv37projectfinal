<%-- 
    Document   : home2
    Created on : Nov 18, 2020, 8:02:05 PM
    Author     : ASUS
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="mvc"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Hiroto Template">
        <meta name="keywords" content="Hiroto, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Hiroto</title>
        <jsp:include page="include/css-header.jsp" />
    </head>
    <body>
        <jsp:include page="include/nav-header.jsp" />
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h2 style="margin-top: 40px">Find Your booking</h2>
                    <c:if test="${type == 'error-find'}">
                        <div class="alert alert-danger">
                            Not found booking, please enter correct information
                        </div>
                    </c:if>
                    <c:if test="${type == 'error-cancel'}">
                        <div class="alert alert-danger">
                            Booking ${bookingNumber} has been cancelled
                        </div>
                    </c:if>
                    <form action="${pageContext.request.contextPath}/find-reservation"
                          method="post"
                          style="margin: 20px">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Booking Number</label>
                            <input class="form-control" value="${booking.bookingNumber}" name="bookingNumber" id="exampleInputPassword1" placeholder="Enter booking number">
                        </div>
                        <button type="submit" class="btn btn-primary">Find booking</button>
                    </form>
                    <c:if test="${type == 'success'}">
                        <div class="alert alert-success">
                            Booking ${bookingNumber} has been cancelled
                        </div>
                    </c:if>
                </div>
                <div class="col-lg-6">
                    <div style="margin-top: 100px">
                        You can do any of the following and more:<br>
                        <span class="ion-android-checkbox-outline" style="color: green"></span> Cancel Booking<br>
                        <span class="ion-android-checkbox-outline" style="color: green"></span> Print Booking
                    </div>
                </div>
            </div>

            <c:if test="${booking != nul}">
                <mvc:form action="${pageContext.request.contextPath}/cancel-booking"
                          method="post">
                    <div class="row">
                        <div class="col-md-6" style="padding-left: 40px">
                            <h4>Reservation Number: ${booking.bookingNumber}</h4>
                            <table class="table" style="width: 60%;margin-left: 40px">
                                <tr>
                                    <td>Number of Rooms:</td>
                                    <td>${booking.bookingDetails.size()}</td>
                                </tr>
                                <tr>
                                    <td>Booking Date:</td>
                                    <td>${booking.bookingDate}</td>
                                </tr>
                                <tr>
                                    <td>Check In: </td>
                                    <td>${booking.checkIn}</td>
                                </tr>
                                <tr>
                                    <td>Check Out: </td>
                                    <td>${booking.checkOut}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-4">
                            <h4>Guest Information</h4>
                            <h5>${booking.fullName}</h5>
                            <h5>email: ${booking.email}</h5>
                            <h5>phone: ${booking.phoneNumber}</h5>
                            <h5>address ${booking.address}</h5>
                        </div>
                        <div class="col-md-2">
                            <input type="hidden" name="bookingId" value="${booking.id}">
                            <c:if test="${expired != 'true'}">
                                <button class="btn btn-danger" type="submit">
                                    cancel booking
                                </button>
                            </c:if>
                            <c:if test="${expired == 'true'}">
                                <h5>you can't cancel booking (expired)</h5>
                            </c:if>
                        </div>
                    </div>
                    <div class="card-body">
                        <!-- PRODUCT -->
                        <c:forEach var="bookingDetail" items="${booking.bookingDetails}">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-2 text-center">
                                    <c:if test="${! empty bookingDetail.room.roomType.images}">
                                        <img class="img-responsive" 
                                             src="<c:url value='/upload/images/${bookingDetail.room.roomType.images.iterator().next().name}' />"
                                             alt="prewiew" width="230" height="150">
                                    </c:if>
                                </div>
                                <div class="col-12 text-sm-center col-sm-12 text-md-left col-md-6">
                                    <h4 class="product-name"><strong>${bookingDetail.room.roomType.name} ROOM</strong></h4>
                                    <h4>
                                        <small>Room Price ${bookingDetail.room.roomType.price}</small>
                                    </h4>
                                </div>
                                <div class="col-12 col-sm-12 text-sm-center col-md-4 text-md-right row">
                                    <div class="col-3 col-sm-3 col-md-6 text-md-right" style="padding-top: 5px">
                                        <h4>${bookingDetail.price} VNĐ</h4>
                                    </div>
                                </div>
                            </div>
                            <h4>Extra Service</h4>
                            <div class="row" style="margin-bottom: 30px">
                                <c:forEach var="service" items="${bookingDetail.services}">
                                    <div class="col-md-4">
                                        <span class="ion-android-checkbox-outline" style="color: green"></span> ${service.name}
                                    </div>
                                </c:forEach>
                            </div> 
                            <hr>
                        </c:forEach>
                        <hr>
                    </div>
                </mvc:form>
            </c:if>
        </div>
        <jsp:include page="include/footer.jsp" />
        <jsp:include page="include/js-page.jsp" />
    </body>
</html>
