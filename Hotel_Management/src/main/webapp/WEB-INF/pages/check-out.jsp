
<%-- 
    Document   : home2
    Created on : Nov 18, 2020, 8:02:05 PM
    Author     : ASUS
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="mvc"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Hiroto Template">
        <meta name="keywords" content="Hiroto, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Check Out</title>
        <jsp:include page="include/css-header.jsp" />
    </head>
    <body>
        <jsp:include page="include/nav-header.jsp" />
        <div class="breadcrumb-option" style='background-image: url("resources/img/breadcrumb-bg.jpg")'>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="breadcrumb__text">
                            <h1>Check Out</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--list room-->
        <div class="container">
            <c:if test="${type == 'success'}">
                <div style="padding: 40px">
                    <h3>Success</h3>
                    Check your email for a booking confirmation. we 'll see you soon !
                </div>
            </c:if>
            <c:if test="${! empty bookingDetails}">
                <div class="row" style="margin-top: 50px">
                    <div class="col-md-4 order-md-2 mb-4">
                        <h4 class="d-flex justify-content-between align-items-center mb-3">
                            <span class="text-muted">Your Booking</span>
                            <span class="badge badge-secondary badge-pill">${bookingDetails.size()}</span>
                        </h4>
                        <ul class="list-group mb-3">
                            <c:forEach var="bookingDetail" items="${bookingDetails}">
                                <li class="list-group-item d-flex justify-content-between lh-condensed">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <c:if test="${!empty bookingDetail.room.roomType.images}">
                                                <img class="img-responsive" 
                                                     src="<c:url value='/upload/images/${bookingDetail.room.roomType.images.iterator().next().name}' />"
                                                     alt="prewiew" >
                                            </c:if>

                                        </div>
                                        <div class="col-md-8">
                                            <h6 class="my-0">${bookingDetail.room.roomType.name}</h6>
                                            <p><small class="text-muted">${bookingDetail.room.roomType.numberOfBed} bed</small></p>
                                            <c:if test="${!empty bookingDetail.services}">
                                                <p><small class="text-muted">${bookingDetail.services.size()} extra service</small></p>
                                            </c:if>
                                        </div>
                                    </div>
                                    <span class="text-muted">${bookingDetail.price}</span>
                                </li>
                            </c:forEach>
                            <li class="list-group-item d-flex justify-content-between">
                                <span>Total (VND)</span>
                                <strong>${totalPrice}</strong>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-8 order-md-1">
                        <h4 class="mb-3">Information</h4>
                        <mvc:form action="${pageContext.request.contextPath}/check-out"
                                  method="post" modelAttribute="booking" >
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <label for="firstName">Full name</label>
                                    <input type="text" class="form-control" id="firstName" name="fullName"
                                           placeholder="full name" value="${booking.fullName}" required>
                                </div>
                            </div>

                            <div class="mb-3">
                                <label for="email">Email</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">@</span>
                                    </div>
                                    <input type="email" value="${booking.email}"  class="form-control" id="email" name="email" placeholder="email" required>
                                </div>
                            </div>

                            <div class="mb-3">
                                <label for="phone">phone</label>
                                <input type="phone" class="form-control" id="phone" value="${booking.phoneNumber}" name="phoneNumber" placeholder="phone number">
                            </div>

                            <div class="mb-3">
                                <label for="address">Address</label>
                                <input type="text" class="form-control" id="address" value="${booking.address}" name="address" placeholder="123 Ha Van Tinh" required>
                            </div>

                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label for="birth">Birth Date</label>
                                        <fmt:formatDate value="${booking.birhtDay}"
                                                                    pattern="yyyy-MM-dd" var="birhtDay" />
                                        <input type="date" class="form-control" pattern="yyyy-MM-dd" id="birth" value="${birhtDay}" name="birhtDay" />
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="gender">Gender</label><br>
                                    <c:forEach items="${gender}" var="g">
                                        <c:if test="${g == booking.gender}">
                                            <label >
                                                <input name="gender" type="radio" checked value="${g}">${g}
                                            </label>
                                        </c:if>
                                        <c:if test="${g != booking.gender}">
                                            <label style="margin-left: 30px">
                                                <input name="gender" type="radio" value="${g}">${g}
                                            </label>
                                        </c:if>
                                    </c:forEach>
                                    <div class="invalid-feedback">
                                        Valid first name is required.
                                    </div>
                                </div>
                            </div>


                            <hr class="mb-4">

                            <h4 class="mb-3">Payment</h4>
                            <c:if test="${messageCredit != null}">
                                <div class="alert alert-danger">
                                    ${messageCredit}
                                </div>
                            </c:if>
                            <c:if test="${messageTrans != null}">
                                <div class="alert alert-danger">
                                    ${messageTrans}
                                </div>
                            </c:if>
                            <div class="d-block my-3">
                                <div class="custom-control custom-radio">
                                    <input id="credit" name="paymentMethod" type="radio" class="custom-control-input" checked required>
                                    <label class="custom-control-label" for="credit">Credit card</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input id="debit" name="paymentMethod" type="radio" class="custom-control-input" required>
                                    <label class="custom-control-label" for="debit">Debit card</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input id="paypal" name="paymentMethod" type="radio" class="custom-control-input" required>
                                    <label class="custom-control-label" for="paypal">Paypal</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="cc-name">Name on card</label>
                                    <input type="text" class="form-control" name="cartName" id="cc-name" placeholder="" required>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="cc-number">Credit card number</label>
                                    <input type="text"  name="cardNumber" class="form-control" id="cc-number" placeholder="" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 mb-3">
                                    <label for="cc-expiration">Expiration</label>
                                    <input type="date" class="form-control" pattern="MM-YY" required
                                           id="cc-expiration" name="expDate"  />
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label for="cvv">CVV</label>
                                    <input type="number" name="code" class="form-control" id="cvv" placeholder="" required>
                                </div>
                            </div>
                            <hr class="mb-4">
                            <button class="btn btn-primary btn-lg btn-block" style="margin-bottom: 50px" type="submit">Check Out</button>
                        </mvc:form>
                    </div>
                </div>
            </c:if>
        </div>
        <jsp:include page="include/footer.jsp" />
        <jsp:include page="include/js-page.jsp" />
    </body>
</html>
