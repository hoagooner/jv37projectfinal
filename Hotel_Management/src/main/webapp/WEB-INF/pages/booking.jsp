<%-- 
    Document   : home2
    Created on : Nov 18, 2020, 8:02:05 PM
    Author     : ASUS
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="mvc"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Hiroto Template">
        <meta name="keywords" content="Hiroto, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Hiroto</title>
        <jsp:include page="include/css-header.jsp" />
    </head>
    <body>
        <jsp:include page="include/nav-header.jsp" />
        <div class="breadcrumb-option" style='background-image: url("resources/img/breadcrumb-bg.jpg")'>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="breadcrumb__text">
                            <h1>Your Booking</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://use.fontawesome.com/c560c025cf.js"></script>
        <div class="container">
            <div class="card shopping-cart">
                <div class="card-header bg-dark text-light">
                    Your Booking
                    <%
                        String url = "checkIn=" + session.getAttribute("checkIn") + "&checkOut=" + session.getAttribute("checkOut") + "&person=" + session.getAttribute("person");
                    %>
                    <a href="${pageContext.request.contextPath}/search?<%=url%>" class="btn btn-outline-info btn-sm pull-right">Continiue Booking</a>
                    <c:if test="${!empty bookingDetails}">
                        <div class="clearfix">
                            <b>Check-in:</b> ${checkIn}  &ensp;&ensp;&ensp;&ensp;
                            <b>Check-out:</b> ${checkOut}
                        </div>
                    </c:if>
                </div>
                <div class="card-body">
                    <!-- PRODUCT -->
                    <c:forEach var="bookingDetail" items="${bookingDetails}">
                        <mvc:form action="${pageContext.request.contextPath}/your-booking/update/${bookingDetail.room.id}"
                                  method="post" modelAttribute="booking" >
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-2 text-center">
                                    <c:if test="${! empty bookingDetail.room.roomType.images}">
                                        <img class="img-responsive" 
                                             src="<c:url value='/upload/images/${bookingDetail.room.roomType.images.iterator().next().name}' />"
                                             alt="prewiew" width="230" height="150">

                                    </c:if>
                                </div>
                                <div class="col-12 text-sm-center col-sm-12 text-md-left col-md-6">
                                    <c:if test="${bookingDetail.discount != 0}">
                                        <button type="button" class="btn btn-outline-danger btn-xs" >
                                            ${bookingDetail.discount}%
                                        </button>
                                    </c:if>
                                    <h4 class="product-name"><strong>${bookingDetail.room.roomType.name} ROOM</strong></h4>
                                    <h4>
                                        <small>Bed: ${bookingDetail.room.roomType.numberOfBed}</small>
                                    </h4>

                                    <h4>
                                        <small>Price: ${bookingDetail.room.roomType.price}/day</small>
                                    </h4>
                                </div>
                                <div class="col-12 col-sm-12 text-sm-center col-md-4 text-md-right row">

                                    <div class="col-3 col-sm-3 col-md-6 text-md-right" style="padding-top: 5px">
                                        <h5><strong>${bookingDetail.price} VNĐ</strong></h5>
                                    </div>

                                    <div class="col-2 col-sm-2 col-md-2 text-right">
                                        <a href="${pageContext.request.contextPath}/your-booking/delete/${bookingDetail.room.id}">
                                            <button type="button" class="btn btn-outline-danger btn-xs" >
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <h4>Extra Service</h4>

                            <div class="row" style="margin-bottom: 30px">
                                <c:forEach var="service" items="${services}">
                                    <%
                                        String str = "";
                                    %>
                                    <c:forEach var="s" items="${bookingDetail.services}">
                                        <c:if test="${s.name == service.name}">
                                            <%
                                                str = "checked";
                                            %>
                                        </c:if>
                                    </c:forEach>
                                    <div class="col-md-4">
                                        <label style="margin-left: 40px">
                                            <input type="checkbox"  <%=str%> name="serviceId" value="${service.id}" >
                                            ${service.name}
                                            (  ${service.price} VNĐ )
                                        </label>
                                    </div>
                                </c:forEach>
                            </div> 
                            <div class="row">
                                <div class="col-md-12" >
                                    <input type="submit" class="btn btn-outline-success btn-xs"style="float: right" value="update">
                                </div>
                            </div>
                            <hr>
                        </mvc:form>
                    </c:forEach>
                    <hr>
                </div>
                <div class="card-footer">
                    <div class="pull-right" style="margin: 10px">
                        <a href="${pageContext.request.contextPath}/check-out" class="btn btn-success pull-right">Checkout</a>
                        <div class="pull-right" style="margin: 10px">
                            Total: ${totalPrice} VNĐ
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <jsp:include page="include/footer.jsp" />
        <jsp:include page="include/js-page.jsp" />
    </body>
</html>
