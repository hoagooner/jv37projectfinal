<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/css/font-awesome.min.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/css/elegant-icons.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/css/nice-select.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/css/jquery-ui.min.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/css/owl.carousel.min.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/css/slicknav.min.css"/>">
<link rel="stylesheet" href="<c:url value="/resources/css/style.css"/>">
 <link rel="stylesheet" href="<c:url value="/resources/fonts/ionicons/css/ionicons.min.css"/>">
 

