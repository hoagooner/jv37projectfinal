<div class="breadcrumb-option" style='background-image: url("resources/img/breadcrumb-bg.jpg")'>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="breadcrumb__text">
                    <h1>Our Room</h1>
                    <div class="breadcrumb__links">
                        <a href="./index.html">Home</a>
                        <span>Rooms</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>