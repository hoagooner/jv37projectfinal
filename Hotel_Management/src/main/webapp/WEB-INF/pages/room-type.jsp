<%-- 
    Document   : home2
    Created on : Nov 18, 2020, 8:02:05 PM
    Author     : ASUS
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Hiroto Template">
        <meta name="keywords" content="Hiroto, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Hiroto</title>
        <jsp:include page="include/css-header.jsp" />
    </head>
    <body>
        <jsp:include page="include/nav-header.jsp" />
        <jsp:include page="include/common-nav.jsp" />
        <!--list room-->
        <section class="rooms spad">
            <div class="container">
                <%
                    int count = 0;
                %>
                <c:forEach var="roomType" items="${roomTypes}">
                    <c:if test="${roomType.status eq 'ACTIVE'}">
                    <div class="row">
                        <%
                            count += 1;
                        %>
                        <div class="col-lg-6 p-0 <%= count % 2 == 0 ? "order-lg-1 order-md-1" : "order-lg-2 order-md-2"%> col-md-6">
                            <div class="room__pic__slider owl-carousel">
                                <c:forEach var="image" items="${roomType.images}">
                                    <div class="room__pic__item" style='background-image: url(<c:url value='/upload/images/${image.name}' />)'></div>
                                </c:forEach>
                            </div>
                        </div>
                        <div class="col-lg-6 p-0  col-md-6 <%= count % 2 == 0 ? "order-lg-2 order-md-2" : "order-lg-1 order-md-1"%>   ">
                            <div class="room__text <%= count % 2 == 0 ? "right__text" : ""%>">
                                <h3>${roomType.name} ROOM</h3>
                                <h2><sup>$</sup>${roomType.price}<span>/day</span></h2>
                                <ul>
                                    <li><span>Size:</span>${roomType.size} m</li>
                                    <li><span>Capacity:</span>Max person ${roomType.occupancy}</li>
                                    <li><span>Bed:</span>${roomType.numberOfBed}</li>
                                    <li><span>Convenient</span>
                                        <c:forEach var="convenient" items="${roomType.convenients}">
                                            ${convenient.name}
                                        </c:forEach>
                                    </li>
                                </ul>
                                <a href="${pageContext.request.contextPath}/room-detail/${roomType.id}">View Details</a>
                            </div>
                        </div>
                    </div>
                    </c:if>
                </c:forEach>
            </div>
        </section>
        <jsp:include page="include/footer.jsp" />
        <jsp:include page="include/js-page.jsp" />
    </body>
</html>
