<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="mvc"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
<%@taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
    <title>Sign Up Form by Colorlib</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="resources/register/fonts/material-icon/css/material-design-iconic-font.min.css">

    <!-- Main css -->
    
    <link rel="stylesheet" href="resources/register/css/style.css">
</head>
<body>

    <div class="main">

        <!-- Sign up form -->
        <section class="signup">
            <div class="container">
                <div class="signup-content">
                    <div class="signup-form">
                        <h2 class="form-title">Sign up</h2>
                        <form action="${pageContext.request.contextPath}/signup" modelAttribute="signup" method="POST">
                            <div  class="caption">
                            <div class="form-group">
                                <label for="name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                                <input type="text" name="fullName" id="fullName" placeholder="Your Name"/>
                            </div>
                            <div class="form-group">
                                <label for="email"><i class="zmdi zmdi-email"></i></label>
                                <input type="email" name="email" id="email" placeholder="Your Email"/>
                            </div>
                            <div class="form-group">
                                <label for="pass"><i class="zmdi zmdi-lock"></i></label>
                                <input type="password" name="password" id="password" placeholder="Password"/>
                            </div>
                            <div class="form-group">
                                <label for="re-pass"><i class="zmdi zmdi-lock-outline"></i></label>
                                <input type="password" name="confirm_password" id="confirm_password" placeholder="Repeat your password"/>
                                <span id='messagePass'></span>
                            </div>
                                <div class="form-group">
                                    <label for="re-pass"><i class="zmdi zmdi-balance"></i></label>
                            <input class="input" type="text" name="address" placeholder="Address" required>
                        </div>
                                <label for="re-pass"><i class="zmdi zmdi-lock-outline"></i></label>
                        <div class="form-group">
                            <label for="re-pass"><i class="zmdi zmdi-daydream-setting"></i></label>
                            <input class="input" type="date" name="birhtDay" placeholder="Birthday" required>
                        </div>
                        <div class="form-group">
                            <label for="re-pass"><i class="zmdi zmdi-phone"></i></label>
                            <input class="input" type="text" name="phoneNumber" placeholder="Phone Number" required>
                        </div>
                            <div class="form-group">
                                <input type="checkbox" name="agree-term" id="agree-term" class="agree-term" />
                                <label for="agree-term" class="label-agree-term"><span><span></span></span>I agree all statements in  <a href="#" class="term-service">Terms of service</a></label>
                            </div>
                            <div class="btn btn-success">
                                    <input type="submit" value="Confirm"/>
                                </div>
                            </div>
 <!--                            <c :forEach var="role" items="$ {roles}">
                           <div class="col-lg-6">
                                <c :if test="$ {role.getRole() == 'ROLE_USER'}">
                                    <div class="checkbox">
                                        <label hidden="">
                                            <input type="checkbox" checked="true" name="role" value="$ {role.id}" hidden="true">
                                            $ {role.getRole()}
                                        </label>
                                    </div>
                                </c :if>
                            </div>
                        </c :forEach>-->
                        <form>
                    </div>
                    <div class="signup-image">
                        <figure><img src="resources/register/images/signup-image.jpg" alt="sing up image"></figure>
                        <a href="#" class="signup-image-link">I am already member</a>
                    </div>
                </div>
            </div>
        </section>

        <!-- Sing in  Form -->
        

    </div>

    <!-- JS -->
    <script src="resources/register/vendor/jquery/jquery.min.js"></script>
    <script src="resources/register/js/main.js"></script>


        <script>
            $('#password, #confirm_password').on('keyup', function () {
                if ($('#password').val() == $('#confirm_password').val()) {
                    $('#messagePass').html('Matching').css('color', 'green');
                } else
                    $('#messagePass').html('Not Matching').css('color', 'red');
            });
        </script>

    <!-- JS -->
    <script src="resources/register/js-crat.jsp"></script>

    </body>
</html>
